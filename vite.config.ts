import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import vuetify from "vite-plugin-vuetify";
import dts from "vite-plugin-dts";
import { resolve } from "path";

export default defineConfig({
    resolve: {
        alias: {
            "@": resolve(__dirname, "lib"),
        },
        extensions: [".ts", ".vue", ".js"],
    },
    build: {
        lib: {
            entry: resolve(__dirname, "lib/index.ts"),
            name: "VToggleTheme",
            fileName: (format) => `v-toggle-theme.${format}.js`,
        },
        rollupOptions: {
            external: ["vue", "vuetify", "sunrise-sunset-js"],
            output: {
                globals: {
                    vue: "Vue",
                    vuetify: "Vuetify",
                    "sunrise-sunset-js": "sunriseSunsetJs",
                },
            },
        },
    },
    plugins: [
        vue(),
        vuetify({ autoImport: true }),
        dts({
            insertTypesEntry: true, // Generiert eine Typendatei für das gesamte Package
            outDir: "dist", // Speichert die Typendateien im `dist`-Verzeichnis
        }),
    ],
});
