# VToggleTheme: Theme Switcher Component

This Vue component automatically switches themes based on the sunrise and sunset times of the user's location, utilizing Vuetify's theming system. Users can also manually toggle the theme, which disables the automatic switching.

## Features

- **Automatic Theme Switching**: Updates the theme based on real-time sunrise/sunset times.
- **Manual Override**: Allows users to manually switch themes, disabling automatic switching.
- **Vuetify Integration**: Seamlessly integrates with Vuetify's theming system.
- **Geo-Location Support**: Uses the browser's geolocation API to determine the user's current coordinates.
- **SSR Compatibility**: Ensures correct operation in both client and server-side environments. 
- **Nuxt3 Compatibility**: Fully compatible with Nuxt3 for seamless integration.

## Requirements

- **Vue 3.x**
- **Vuetify 3.x**
- **@mdi/js**: Only required for mdi/js icon support; the fallback is @mdi/font.
- **sunrise-sunset-js**: Required for real-time sunrise/sunset times. 

## Installation

Install the component via npm:
@mdi/js is only required if you prefer not to use @mdi/font

```bash
npm install v-toggle-theme sunrise-sunset-js @mdi/js --save
```

Ensure Vue 3.x, Vuetify 3.x, and @mdi/js are set up in your project:

```bash
npm install vue@latest vuetify@latest
```

## Usage

### Props

- **`themeNameLight`** (`String`): Name of the light theme. Default: 'light'.
    - Example: `:themeNameLight="'myLightTheme'"`
- **`themeNameDark`** (`String`): Name of the dark theme. Default: 'dark'.
    - Example: `:themeNameDark="'myDarkTheme'"`
- **`fallbackLocation`** (`Object`): Default coordinates if geolocation fails. Default: `{ lat: 47.36667, lng: 8.55 }` (Zurich).
    - Example: `:fallbackLocation="{ lat: 40.7128, lng: -74.0060 }"` (New York)
- **`tooltip`** (`Boolean`): Enables tooltips. Default: false.
    - Example: `:tooltip="true"`
- **`tipDark`** (`String`): Tooltip text for dark mode. Default: 'Dark mode'.
    - Example: `:tipDark="'Activate dark mode'"`
- **`tipLight`** (`String`): Tooltip text for light mode. Default: 'Light mode'.
    - Example: `:tipLight="'Activate light mode'"`
- **`automatic`** (`Boolean`): Enables automatic theme switching. Default: true.
    - Example: `:automatic="false"`
- **`mdiJsIcons`** (`Boolean`): Use @mdi/js icons. Default: false.
    - Example: `:mdi-js-icons="true"`

### Integration

#### Options API

```vue
<script>
  import VToggleTheme from 'v-toggle-theme';

  export default {
    components: {
      VToggleTheme
    }
  }
</script>
<template>
  <v-toggle-theme color="primary" :tooltip="true" />
</template>
```

#### Composition API

```vue
<script setup>
  import VToggleTheme from 'v-toggle-theme';
</script>

<template>
  <v-toggle-theme color="primary" :tooltip="true" />
</template>
```

### Events

- **`switchManually`**: Toggles the theme manually and stops automatic switching.

## Customization

Customize the switch appearance using Vuetify's v-btn properties. Modify `themeNameLight` and `themeNameDark` according to your Vuetify theme settings for deeper integration.

## License

Licensed under the GNU Lesser General Public License as published by the Free Software Foundation, either version 2.1 of the License, or (at your option) any later version.

## Acknowledgements

Thanks to **[Matt Kane (@ascorbic)](https://twitter.com/ascorbic)** for creating the `sunrise-sunset-js` package, a crucial tool for this project.
