import { Fragment as st, reactive as le, computed as g, watchEffect as ce, toRefs as tn, capitalize as Un, shallowRef as U, warn as lt, getCurrentInstance as Yn, inject as J, ref as B, unref as K, provide as fe, defineComponent as Ze, isRef as ue, createVNode as p, toRef as j, watch as I, onScopeDispose as q, effectScope as ut, toRaw as nn, onBeforeUnmount as Ce, onMounted as ct, onUpdated as Xn, mergeProps as z, Text as Kn, readonly as rn, resolveDynamicComponent as Jn, nextTick as de, withDirectives as on, h as Zn, TransitionGroup as Qn, Transition as an, Teleport as ei, vShow as ti, resolveDirective as ni, onUnmounted as ii, openBlock as ri, createBlock as oi, withCtx as ai } from "vue";
import { getSunrise as ze, getSunset as xt } from "sunrise-sunset-js";
import { useTheme as si } from "vuetify";
var li = "M17.75,4.09L15.22,6.03L16.13,9.09L13.5,7.28L10.87,9.09L11.78,6.03L9.25,4.09L12.44,4L13.5,1L14.56,4L17.75,4.09M21.25,11L19.61,12.25L20.2,14.23L18.5,13.06L16.8,14.23L17.39,12.25L15.75,11L17.81,10.95L18.5,9L19.19,10.95L21.25,11M18.97,15.95C19.8,15.87 20.69,17.05 20.16,17.8C19.84,18.25 19.5,18.67 19.08,19.07C15.17,23 8.84,23 4.94,19.07C1.03,15.17 1.03,8.83 4.94,4.93C5.34,4.53 5.76,4.17 6.21,3.85C6.96,3.32 8.14,4.21 8.06,5.04C7.79,7.9 8.75,10.87 10.95,13.06C13.14,15.26 16.1,16.22 18.97,15.95M17.33,17.97C14.5,17.81 11.7,16.64 9.53,14.5C7.36,12.31 6.2,9.5 6.04,6.68C3.23,9.82 3.34,14.64 6.35,17.66C9.37,20.67 14.19,20.78 17.33,17.97Z", ui = "M12,7A5,5 0 0,1 17,12A5,5 0 0,1 12,17A5,5 0 0,1 7,12A5,5 0 0,1 12,7M12,9A3,3 0 0,0 9,12A3,3 0 0,0 12,15A3,3 0 0,0 15,12A3,3 0 0,0 12,9M12,2L14.39,5.42C13.65,5.15 12.84,5 12,5C11.16,5 10.35,5.15 9.61,5.42L12,2M3.34,7L7.5,6.65C6.9,7.16 6.36,7.78 5.94,8.5C5.5,9.24 5.25,10 5.11,10.79L3.34,7M3.36,17L5.12,13.23C5.26,14 5.53,14.78 5.95,15.5C6.37,16.24 6.91,16.86 7.5,17.37L3.36,17M20.65,7L18.88,10.79C18.74,10 18.47,9.23 18.05,8.5C17.63,7.78 17.1,7.15 16.5,6.64L20.65,7M20.64,17L16.5,17.36C17.09,16.85 17.62,16.22 18.04,15.5C18.46,14.77 18.73,14 18.87,13.21L20.64,17M12,22L9.59,18.56C10.33,18.83 11.14,19 12,19C12.82,19 13.63,18.83 14.37,18.56L12,22Z";
const G = typeof window < "u", ci = G && "IntersectionObserver" in window;
function ft(e, t) {
  if (e === t) return !0;
  if (e instanceof Date && t instanceof Date && e.getTime() !== t.getTime() || e !== Object(e) || t !== Object(t))
    return !1;
  const n = Object.keys(e);
  return n.length !== Object.keys(t).length ? !1 : n.every((i) => ft(e[i], t[i]));
}
function L(e) {
  let t = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : "px";
  if (!(e == null || e === ""))
    return isNaN(+e) ? String(e) : isFinite(+e) ? `${Number(e)}${t}` : void 0;
}
function fi(e) {
  return e !== null && typeof e == "object" && !Array.isArray(e);
}
function kt(e) {
  let t;
  return e !== null && typeof e == "object" && ((t = Object.getPrototypeOf(e)) === Object.prototype || t === null);
}
function di(e) {
  if (e && "$el" in e) {
    const t = e.$el;
    return (t == null ? void 0 : t.nodeType) === Node.TEXT_NODE ? t.nextElementSibling : t;
  }
  return e;
}
const Et = Object.freeze({
  enter: 13,
  tab: 9,
  delete: 46,
  esc: 27,
  space: 32,
  up: 38,
  down: 40,
  left: 37,
  right: 39,
  end: 35,
  home: 36,
  del: 46,
  backspace: 8,
  insert: 45,
  pageup: 33,
  pagedown: 34,
  shift: 16
});
function He(e, t) {
  return t.every((n) => e.hasOwnProperty(n));
}
function vi(e, t) {
  const n = {}, i = new Set(Object.keys(e));
  for (const r of t)
    i.has(r) && (n[r] = e[r]);
  return n;
}
function mi(e, t) {
  const n = {
    ...e
  };
  return t.forEach((i) => delete n[i]), n;
}
const gi = /^on[^a-z]/, sn = (e) => gi.test(e);
function hi(e) {
  return e == null ? [] : Array.isArray(e) ? e : [e];
}
function Pt(e) {
  let t = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : 0, n = arguments.length > 2 && arguments[2] !== void 0 ? arguments[2] : 1;
  return Math.max(t, Math.min(n, e));
}
function Ot(e, t) {
  let n = arguments.length > 2 && arguments[2] !== void 0 ? arguments[2] : "0";
  return e + n.repeat(Math.max(0, t - e.length));
}
function yi(e) {
  let t = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : 1;
  const n = [];
  let i = 0;
  for (; i < e.length; )
    n.push(e.substr(i, t)), i += t;
  return n;
}
function ae() {
  let e = arguments.length > 0 && arguments[0] !== void 0 ? arguments[0] : {}, t = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : {}, n = arguments.length > 2 ? arguments[2] : void 0;
  const i = {};
  for (const r in e)
    i[r] = e[r];
  for (const r in t) {
    const o = e[r], a = t[r];
    if (kt(o) && kt(a)) {
      i[r] = ae(o, a, n);
      continue;
    }
    if (n && Array.isArray(o) && Array.isArray(a)) {
      i[r] = n(o, a);
      continue;
    }
    i[r] = a;
  }
  return i;
}
function ln(e) {
  return e.map((t) => t.type === st ? ln(t.children) : t).flat();
}
function Q() {
  let e = arguments.length > 0 && arguments[0] !== void 0 ? arguments[0] : "";
  if (Q.cache.has(e)) return Q.cache.get(e);
  const t = e.replace(/[^a-z]/gi, "-").replace(/\B([A-Z])/g, "-$1").toLowerCase();
  return Q.cache.set(e, t), t;
}
Q.cache = /* @__PURE__ */ new Map();
function he(e, t) {
  if (!t || typeof t != "object") return [];
  if (Array.isArray(t))
    return t.map((n) => he(e, n)).flat(1);
  if (t.suspense)
    return he(e, t.ssContent);
  if (Array.isArray(t.children))
    return t.children.map((n) => he(e, n)).flat(1);
  if (t.component) {
    if (Object.getOwnPropertySymbols(t.component.provides).includes(e))
      return [t.component];
    if (t.component.subTree)
      return he(e, t.component.subTree).flat(1);
  }
  return [];
}
function dt(e) {
  const t = le({}), n = g(e);
  return ce(() => {
    for (const i in n.value)
      t[i] = n.value[i];
  }, {
    flush: "sync"
  }), tn(t);
}
function Be(e, t) {
  return e.includes(t);
}
function un(e) {
  return e[2].toLowerCase() + e.slice(3);
}
function Tt(e, t) {
  return t = "on" + Un(t), !!(e[t] || e[`${t}Once`] || e[`${t}Capture`] || e[`${t}OnceCapture`] || e[`${t}CaptureOnce`]);
}
function bi(e, t) {
  if (!(G && typeof CSS < "u" && typeof CSS.supports < "u" && CSS.supports(`selector(${t})`))) return null;
  try {
    return !!e && e.matches(t);
  } catch {
    return null;
  }
}
function pi(e, t) {
  if (!G || e === 0)
    return t(), () => {
    };
  const n = window.setTimeout(t, e);
  return () => window.clearTimeout(n);
}
function Qe() {
  const e = U(), t = (n) => {
    e.value = n;
  };
  return Object.defineProperty(t, "value", {
    enumerable: !0,
    get: () => e.value,
    set: (n) => e.value = n
  }), Object.defineProperty(t, "el", {
    enumerable: !0,
    get: () => di(e.value)
  }), t;
}
const cn = ["top", "bottom"], wi = ["start", "end", "left", "right"];
function et(e, t) {
  let [n, i] = e.split(" ");
  return i || (i = Be(cn, n) ? "start" : Be(wi, n) ? "top" : "center"), {
    side: Lt(n, t),
    align: Lt(i, t)
  };
}
function Lt(e, t) {
  return e === "start" ? t ? "right" : "left" : e === "end" ? t ? "left" : "right" : e;
}
function je(e) {
  return {
    side: {
      center: "center",
      top: "bottom",
      bottom: "top",
      left: "right",
      right: "left"
    }[e.side],
    align: e.align
  };
}
function Ge(e) {
  return {
    side: e.side,
    align: {
      center: "center",
      top: "bottom",
      bottom: "top",
      left: "right",
      right: "left"
    }[e.align]
  };
}
function _t(e) {
  return {
    side: e.align,
    align: e.side
  };
}
function At(e) {
  return Be(cn, e.side) ? "y" : "x";
}
class ee {
  constructor(t) {
    let {
      x: n,
      y: i,
      width: r,
      height: o
    } = t;
    this.x = n, this.y = i, this.width = r, this.height = o;
  }
  get top() {
    return this.y;
  }
  get bottom() {
    return this.y + this.height;
  }
  get left() {
    return this.x;
  }
  get right() {
    return this.x + this.width;
  }
}
function Bt(e, t) {
  return {
    x: {
      before: Math.max(0, t.left - e.left),
      after: Math.max(0, e.right - t.right)
    },
    y: {
      before: Math.max(0, t.top - e.top),
      after: Math.max(0, e.bottom - t.bottom)
    }
  };
}
function Ci(e) {
  return Array.isArray(e) ? new ee({
    x: e[0],
    y: e[1],
    width: 0,
    height: 0
  }) : e.getBoundingClientRect();
}
function Si(e) {
  const t = e.getBoundingClientRect(), n = getComputedStyle(e), i = n.transform;
  if (i) {
    let r, o, a, s, l;
    if (i.startsWith("matrix3d("))
      r = i.slice(9, -1).split(/, /), o = +r[0], a = +r[5], s = +r[12], l = +r[13];
    else if (i.startsWith("matrix("))
      r = i.slice(7, -1).split(/, /), o = +r[0], a = +r[3], s = +r[4], l = +r[5];
    else
      return new ee(t);
    const u = n.transformOrigin, f = t.x - s - (1 - o) * parseFloat(u), v = t.y - l - (1 - a) * parseFloat(u.slice(u.indexOf(" ") + 1)), c = o ? t.width / o : e.offsetWidth + 1, m = a ? t.height / a : e.offsetHeight + 1;
    return new ee({
      x: f,
      y: v,
      width: c,
      height: m
    });
  } else
    return new ee(t);
}
function xi(e, t, n) {
  if (typeof e.animate > "u") return {
    finished: Promise.resolve()
  };
  let i;
  try {
    i = e.animate(t, n);
  } catch {
    return {
      finished: Promise.resolve()
    };
  }
  return typeof i.finished > "u" && (i.finished = new Promise((r) => {
    i.onfinish = () => {
      r(i);
    };
  })), i;
}
const Te = /* @__PURE__ */ new WeakMap();
function ki(e, t) {
  Object.keys(t).forEach((n) => {
    if (sn(n)) {
      const i = un(n), r = Te.get(e);
      if (t[n] == null)
        r == null || r.forEach((o) => {
          const [a, s] = o;
          a === i && (e.removeEventListener(i, s), r.delete(o));
        });
      else if (!r || ![...r].some((o) => o[0] === i && o[1] === t[n])) {
        e.addEventListener(i, t[n]);
        const o = r || /* @__PURE__ */ new Set();
        o.add([i, t[n]]), Te.has(e) || Te.set(e, o);
      }
    } else
      t[n] == null ? e.removeAttribute(n) : e.setAttribute(n, t[n]);
  });
}
function Ei(e, t) {
  Object.keys(t).forEach((n) => {
    if (sn(n)) {
      const i = un(n), r = Te.get(e);
      r == null || r.forEach((o) => {
        const [a, s] = o;
        a === i && (e.removeEventListener(i, s), r.delete(o));
      });
    } else
      e.removeAttribute(n);
  });
}
const oe = 2.4, It = 0.2126729, Rt = 0.7151522, Vt = 0.072175, Pi = 0.55, Oi = 0.58, Ti = 0.57, Li = 0.62, Pe = 0.03, $t = 1.45, _i = 5e-4, Ai = 1.25, Bi = 1.25, Nt = 0.078, Mt = 12.82051282051282, Oe = 0.06, Ft = 1e-3;
function Dt(e, t) {
  const n = (e.r / 255) ** oe, i = (e.g / 255) ** oe, r = (e.b / 255) ** oe, o = (t.r / 255) ** oe, a = (t.g / 255) ** oe, s = (t.b / 255) ** oe;
  let l = n * It + i * Rt + r * Vt, u = o * It + a * Rt + s * Vt;
  if (l <= Pe && (l += (Pe - l) ** $t), u <= Pe && (u += (Pe - u) ** $t), Math.abs(u - l) < _i) return 0;
  let f;
  if (u > l) {
    const v = (u ** Pi - l ** Oi) * Ai;
    f = v < Ft ? 0 : v < Nt ? v - v * Mt * Oe : v - Oe;
  } else {
    const v = (u ** Li - l ** Ti) * Bi;
    f = v > -Ft ? 0 : v > -Nt ? v - v * Mt * Oe : v + Oe;
  }
  return f * 100;
}
function se(e) {
  lt(`Vuetify: ${e}`);
}
function Ii(e) {
  lt(`Vuetify error: ${e}`);
}
function tt(e) {
  return !!e && /^(#|var\(--|(rgb|hsl)a?\()/.test(e);
}
function Ri(e) {
  return tt(e) && !/^((rgb|hsl)a?\()?var\(--/.test(e);
}
const Wt = /^(?<fn>(?:rgb|hsl)a?)\((?<values>.+)\)/, Vi = {
  rgb: (e, t, n, i) => ({
    r: e,
    g: t,
    b: n,
    a: i
  }),
  rgba: (e, t, n, i) => ({
    r: e,
    g: t,
    b: n,
    a: i
  }),
  hsl: (e, t, n, i) => zt({
    h: e,
    s: t,
    l: n,
    a: i
  }),
  hsla: (e, t, n, i) => zt({
    h: e,
    s: t,
    l: n,
    a: i
  }),
  hsv: (e, t, n, i) => be({
    h: e,
    s: t,
    v: n,
    a: i
  }),
  hsva: (e, t, n, i) => be({
    h: e,
    s: t,
    v: n,
    a: i
  })
};
function ye(e) {
  if (typeof e == "number")
    return (isNaN(e) || e < 0 || e > 16777215) && se(`'${e}' is not a valid hex color`), {
      r: (e & 16711680) >> 16,
      g: (e & 65280) >> 8,
      b: e & 255
    };
  if (typeof e == "string" && Wt.test(e)) {
    const {
      groups: t
    } = e.match(Wt), {
      fn: n,
      values: i
    } = t, r = i.split(/,\s*/).map((o) => o.endsWith("%") && ["hsl", "hsla", "hsv", "hsva"].includes(n) ? parseFloat(o) / 100 : parseFloat(o));
    return Vi[n](...r);
  } else if (typeof e == "string") {
    let t = e.startsWith("#") ? e.slice(1) : e;
    [3, 4].includes(t.length) ? t = t.split("").map((i) => i + i).join("") : [6, 8].includes(t.length) || se(`'${e}' is not a valid hex(a) color`);
    const n = parseInt(t, 16);
    return (isNaN(n) || n < 0 || n > 4294967295) && se(`'${e}' is not a valid hex(a) color`), $i(t);
  } else if (typeof e == "object") {
    if (He(e, ["r", "g", "b"]))
      return e;
    if (He(e, ["h", "s", "l"]))
      return be(fn(e));
    if (He(e, ["h", "s", "v"]))
      return be(e);
  }
  throw new TypeError(`Invalid color: ${e == null ? e : String(e) || e.constructor.name}
Expected #hex, #hexa, rgb(), rgba(), hsl(), hsla(), object or number`);
}
function be(e) {
  const {
    h: t,
    s: n,
    v: i,
    a: r
  } = e, o = (s) => {
    const l = (s + t / 60) % 6;
    return i - i * n * Math.max(Math.min(l, 4 - l, 1), 0);
  }, a = [o(5), o(3), o(1)].map((s) => Math.round(s * 255));
  return {
    r: a[0],
    g: a[1],
    b: a[2],
    a: r
  };
}
function zt(e) {
  return be(fn(e));
}
function fn(e) {
  const {
    h: t,
    s: n,
    l: i,
    a: r
  } = e, o = i + n * Math.min(i, 1 - i), a = o === 0 ? 0 : 2 - 2 * i / o;
  return {
    h: t,
    s: a,
    v: o,
    a: r
  };
}
function $i(e) {
  e = Ni(e);
  let [t, n, i, r] = yi(e, 2).map((o) => parseInt(o, 16));
  return r = r === void 0 ? r : r / 255, {
    r: t,
    g: n,
    b: i,
    a: r
  };
}
function Ni(e) {
  return e.startsWith("#") && (e = e.slice(1)), e = e.replace(/([^0-9a-f])/gi, "F"), (e.length === 3 || e.length === 4) && (e = e.split("").map((t) => t + t).join("")), e.length !== 6 && (e = Ot(Ot(e, 6), 8, "F")), e;
}
function Mi(e) {
  const t = Math.abs(Dt(ye(0), ye(e)));
  return Math.abs(Dt(ye(16777215), ye(e))) > Math.min(t, 50) ? "#fff" : "#000";
}
function S(e, t) {
  return (n) => Object.keys(e).reduce((i, r) => {
    const a = typeof e[r] == "object" && e[r] != null && !Array.isArray(e[r]) ? e[r] : {
      type: e[r]
    };
    return n && r in n ? i[r] = {
      ...a,
      default: n[r]
    } : i[r] = a, t && !i[r].source && (i[r].source = t), i;
  }, {});
}
const Se = S({
  class: [String, Array, Object],
  style: {
    type: [String, Array, Object],
    default: null
  }
}, "component");
function N(e, t) {
  const n = Yn();
  if (!n)
    throw new Error(`[Vuetify] ${e} must be called from inside a setup function`);
  return n;
}
function Z() {
  let e = arguments.length > 0 && arguments[0] !== void 0 ? arguments[0] : "composables";
  const t = N(e).type;
  return Q((t == null ? void 0 : t.aliasName) || (t == null ? void 0 : t.name));
}
let dn = 0, Le = /* @__PURE__ */ new WeakMap();
function vt() {
  const e = N("getUid");
  if (Le.has(e)) return Le.get(e);
  {
    const t = dn++;
    return Le.set(e, t), t;
  }
}
vt.reset = () => {
  dn = 0, Le = /* @__PURE__ */ new WeakMap();
};
function Fi(e) {
  let t = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : N("injectSelf");
  const {
    provides: n
  } = t;
  if (n && e in n)
    return n[e];
}
const Ie = Symbol.for("vuetify:defaults");
function mt() {
  const e = J(Ie);
  if (!e) throw new Error("[Vuetify] Could not find defaults instance");
  return e;
}
function vn(e, t) {
  const n = mt(), i = B(e), r = g(() => {
    if (K(t == null ? void 0 : t.disabled)) return n.value;
    const a = K(t == null ? void 0 : t.scoped), s = K(t == null ? void 0 : t.reset), l = K(t == null ? void 0 : t.root);
    if (i.value == null && !(a || s || l)) return n.value;
    let u = ae(i.value, {
      prev: n.value
    });
    if (a) return u;
    if (s || l) {
      const f = Number(s || 1 / 0);
      for (let v = 0; v <= f && !(!u || !("prev" in u)); v++)
        u = u.prev;
      return u && typeof l == "string" && l in u && (u = ae(ae(u, {
        prev: u
      }), u[l])), u;
    }
    return u.prev ? ae(u.prev, u) : u;
  });
  return fe(Ie, r), r;
}
function Di(e, t) {
  var n, i;
  return typeof ((n = e.props) == null ? void 0 : n[t]) < "u" || typeof ((i = e.props) == null ? void 0 : i[Q(t)]) < "u";
}
function Wi() {
  let e = arguments.length > 0 && arguments[0] !== void 0 ? arguments[0] : {}, t = arguments.length > 1 ? arguments[1] : void 0, n = arguments.length > 2 && arguments[2] !== void 0 ? arguments[2] : mt();
  const i = N("useDefaults");
  if (t = t ?? i.type.name ?? i.type.__name, !t)
    throw new Error("[Vuetify] Could not determine component name");
  const r = g(() => {
    var l;
    return (l = n.value) == null ? void 0 : l[e._as ?? t];
  }), o = new Proxy(e, {
    get(l, u) {
      var v, c, m, h, d, b, w;
      const f = Reflect.get(l, u);
      return u === "class" || u === "style" ? [(v = r.value) == null ? void 0 : v[u], f].filter((E) => E != null) : typeof u == "string" && !Di(i.vnode, u) ? ((c = r.value) == null ? void 0 : c[u]) !== void 0 ? (m = r.value) == null ? void 0 : m[u] : ((d = (h = n.value) == null ? void 0 : h.global) == null ? void 0 : d[u]) !== void 0 ? (w = (b = n.value) == null ? void 0 : b.global) == null ? void 0 : w[u] : f : f;
    }
  }), a = U();
  ce(() => {
    if (r.value) {
      const l = Object.entries(r.value).filter((u) => {
        let [f] = u;
        return f.startsWith(f[0].toUpperCase());
      });
      a.value = l.length ? Object.fromEntries(l) : void 0;
    } else
      a.value = void 0;
  });
  function s() {
    const l = Fi(Ie, i);
    fe(Ie, g(() => a.value ? ae((l == null ? void 0 : l.value) ?? {}, a.value) : l == null ? void 0 : l.value));
  }
  return {
    props: o,
    provideSubDefaults: s
  };
}
function Me(e) {
  if (e._setup = e._setup ?? e.setup, !e.name)
    return se("The component is missing an explicit name, unable to generate default prop value"), e;
  if (e._setup) {
    e.props = S(e.props ?? {}, e.name)();
    const t = Object.keys(e.props).filter((n) => n !== "class" && n !== "style");
    e.filterProps = function(i) {
      return vi(i, t);
    }, e.props._as = String, e.setup = function(i, r) {
      const o = mt();
      if (!o.value) return e._setup(i, r);
      const {
        props: a,
        provideSubDefaults: s
      } = Wi(i, i._as ?? e.name, o), l = e._setup(a, r);
      return s(), l;
    };
  }
  return e;
}
function Y() {
  let e = arguments.length > 0 && arguments[0] !== void 0 ? arguments[0] : !0;
  return (t) => (e ? Me : Ze)(t);
}
function mn(e) {
  if (typeof e.getRootNode != "function") {
    for (; e.parentNode; ) e = e.parentNode;
    return e !== document ? null : document;
  }
  const t = e.getRootNode();
  return t !== document && t.getRootNode({
    composed: !0
  }) !== document ? null : t;
}
const zi = "cubic-bezier(0.4, 0, 0.2, 1)";
function Hi(e) {
  let t = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : !1;
  for (; e; ) {
    if (t ? ji(e) : gt(e)) return e;
    e = e.parentElement;
  }
  return document.scrollingElement;
}
function Re(e, t) {
  const n = [];
  if (t && e && !t.contains(e)) return n;
  for (; e && (gt(e) && n.push(e), e !== t); )
    e = e.parentElement;
  return n;
}
function gt(e) {
  if (!e || e.nodeType !== Node.ELEMENT_NODE) return !1;
  const t = window.getComputedStyle(e);
  return t.overflowY === "scroll" || t.overflowY === "auto" && e.scrollHeight > e.clientHeight;
}
function ji(e) {
  if (!e || e.nodeType !== Node.ELEMENT_NODE) return !1;
  const t = window.getComputedStyle(e);
  return ["scroll", "auto"].includes(t.overflowY);
}
function Gi(e) {
  for (; e; ) {
    if (window.getComputedStyle(e).position === "fixed")
      return !0;
    e = e.offsetParent;
  }
  return !1;
}
function te(e) {
  const t = N("useRender");
  t.render = e;
}
const gn = S({
  border: [Boolean, Number, String]
}, "border");
function hn(e) {
  let t = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : Z();
  return {
    borderClasses: g(() => {
      const i = ue(e) ? e.value : e.border, r = [];
      if (i === !0 || i === "")
        r.push(`${t}--border`);
      else if (typeof i == "string" || i === 0)
        for (const o of String(i).split(" "))
          r.push(`border-${o}`);
      return r;
    })
  };
}
const qi = [null, "default", "comfortable", "compact"], yn = S({
  density: {
    type: String,
    default: "default",
    validator: (e) => qi.includes(e)
  }
}, "density");
function bn(e) {
  let t = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : Z();
  return {
    densityClasses: g(() => `${t}--density-${e.density}`)
  };
}
const pn = S({
  elevation: {
    type: [Number, String],
    validator(e) {
      const t = parseInt(e);
      return !isNaN(t) && t >= 0 && // Material Design has a maximum elevation of 24
      // https://material.io/design/environment/elevation.html#default-elevations
      t <= 24;
    }
  }
}, "elevation");
function wn(e) {
  return {
    elevationClasses: g(() => {
      const n = ue(e) ? e.value : e.elevation, i = [];
      return n == null || i.push(`elevation-${n}`), i;
    })
  };
}
const Cn = S({
  rounded: {
    type: [Boolean, Number, String],
    default: void 0
  },
  tile: Boolean
}, "rounded");
function Sn(e) {
  let t = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : Z();
  return {
    roundedClasses: g(() => {
      const i = ue(e) ? e.value : e.rounded, r = ue(e) ? e.value : e.tile, o = [];
      if (i === !0 || i === "")
        o.push(`${t}--rounded`);
      else if (typeof i == "string" || i === 0)
        for (const a of String(i).split(" "))
          o.push(`rounded-${a}`);
      else (r || i === !1) && o.push("rounded-0");
      return o;
    })
  };
}
const Fe = S({
  tag: {
    type: String,
    default: "div"
  }
}, "tag"), Ht = Symbol.for("vuetify:theme"), xe = S({
  theme: String
}, "theme");
function ke(e) {
  N("provideTheme");
  const t = J(Ht, null);
  if (!t) throw new Error("Could not find Vuetify theme injection");
  const n = g(() => e.theme ?? t.name.value), i = g(() => t.themes.value[n.value]), r = g(() => t.isDisabled ? void 0 : `v-theme--${n.value}`), o = {
    ...t,
    name: n,
    current: i,
    themeClasses: r
  };
  return fe(Ht, o), o;
}
function ht(e) {
  return dt(() => {
    const t = [], n = {};
    if (e.value.background)
      if (tt(e.value.background)) {
        if (n.backgroundColor = e.value.background, !e.value.text && Ri(e.value.background)) {
          const i = ye(e.value.background);
          if (i.a == null || i.a === 1) {
            const r = Mi(i);
            n.color = r, n.caretColor = r;
          }
        }
      } else
        t.push(`bg-${e.value.background}`);
    return e.value.text && (tt(e.value.text) ? (n.color = e.value.text, n.caretColor = e.value.text) : t.push(`text-${e.value.text}`)), {
      colorClasses: t,
      colorStyles: n
    };
  });
}
function nt(e, t) {
  const n = g(() => ({
    text: ue(e) ? e.value : null
  })), {
    colorClasses: i,
    colorStyles: r
  } = ht(n);
  return {
    textColorClasses: i,
    textColorStyles: r
  };
}
function Ui(e, t) {
  const n = g(() => ({
    background: ue(e) ? e.value : null
  })), {
    colorClasses: i,
    colorStyles: r
  } = ht(n);
  return {
    backgroundColorClasses: i,
    backgroundColorStyles: r
  };
}
const Yi = ["elevated", "flat", "tonal", "outlined", "text", "plain"];
function Xi(e, t) {
  return p(st, null, [p("span", {
    key: "overlay",
    class: `${t}__overlay`
  }, null), p("span", {
    key: "underlay",
    class: `${t}__underlay`
  }, null)]);
}
const xn = S({
  color: String,
  variant: {
    type: String,
    default: "elevated",
    validator: (e) => Yi.includes(e)
  }
}, "variant");
function Ki(e) {
  let t = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : Z();
  const n = g(() => {
    const {
      variant: o
    } = K(e);
    return `${t}--variant-${o}`;
  }), {
    colorClasses: i,
    colorStyles: r
  } = ht(g(() => {
    const {
      variant: o,
      color: a
    } = K(e);
    return {
      [["elevated", "flat"].includes(o) ? "background" : "text"]: a
    };
  }));
  return {
    colorClasses: i,
    colorStyles: r,
    variantClasses: n
  };
}
const kn = S({
  baseColor: String,
  divided: Boolean,
  ...gn(),
  ...Se(),
  ...yn(),
  ...pn(),
  ...Cn(),
  ...Fe(),
  ...xe(),
  ...xn()
}, "VBtnGroup"), jt = Y()({
  name: "VBtnGroup",
  props: kn(),
  setup(e, t) {
    let {
      slots: n
    } = t;
    const {
      themeClasses: i
    } = ke(e), {
      densityClasses: r
    } = bn(e), {
      borderClasses: o
    } = hn(e), {
      elevationClasses: a
    } = wn(e), {
      roundedClasses: s
    } = Sn(e);
    vn({
      VBtn: {
        height: "auto",
        baseColor: j(e, "baseColor"),
        color: j(e, "color"),
        density: j(e, "density"),
        flat: !0,
        variant: j(e, "variant")
      }
    }), te(() => p(e.tag, {
      class: ["v-btn-group", {
        "v-btn-group--divided": e.divided
      }, i.value, o.value, r.value, a.value, s.value, e.class],
      style: e.style
    }, n));
  }
});
function De(e, t) {
  let n;
  function i() {
    n = ut(), n.run(() => t.length ? t(() => {
      n == null || n.stop(), i();
    }) : t());
  }
  I(e, (r) => {
    r && !n ? i() : r || (n == null || n.stop(), n = void 0);
  }, {
    immediate: !0
  }), q(() => {
    n == null || n.stop();
  });
}
function yt(e, t, n) {
  let i = arguments.length > 3 && arguments[3] !== void 0 ? arguments[3] : (v) => v, r = arguments.length > 4 && arguments[4] !== void 0 ? arguments[4] : (v) => v;
  const o = N("useProxiedModel"), a = B(e[t] !== void 0 ? e[t] : n), s = Q(t), u = s !== t ? g(() => {
    var v, c, m, h;
    return e[t], !!(((v = o.vnode.props) != null && v.hasOwnProperty(t) || (c = o.vnode.props) != null && c.hasOwnProperty(s)) && ((m = o.vnode.props) != null && m.hasOwnProperty(`onUpdate:${t}`) || (h = o.vnode.props) != null && h.hasOwnProperty(`onUpdate:${s}`)));
  }) : g(() => {
    var v, c;
    return e[t], !!((v = o.vnode.props) != null && v.hasOwnProperty(t) && ((c = o.vnode.props) != null && c.hasOwnProperty(`onUpdate:${t}`)));
  });
  De(() => !u.value, () => {
    I(() => e[t], (v) => {
      a.value = v;
    });
  });
  const f = g({
    get() {
      const v = e[t];
      return i(u.value ? v : a.value);
    },
    set(v) {
      const c = r(v), m = nn(u.value ? e[t] : a.value);
      m === c || i(m) === v || (a.value = c, o == null || o.emit(`update:${t}`, c));
    }
  });
  return Object.defineProperty(f, "externalValue", {
    get: () => u.value ? e[t] : a.value
  }), f;
}
const Ji = S({
  modelValue: {
    type: null,
    default: void 0
  },
  multiple: Boolean,
  mandatory: [Boolean, String],
  max: Number,
  selectedClass: String,
  disabled: Boolean
}, "group"), Zi = S({
  value: null,
  disabled: Boolean,
  selectedClass: String
}, "group-item");
function Qi(e, t) {
  let n = arguments.length > 2 && arguments[2] !== void 0 ? arguments[2] : !0;
  const i = N("useGroupItem");
  if (!i)
    throw new Error("[Vuetify] useGroupItem composable must be used inside a component setup function");
  const r = vt();
  fe(Symbol.for(`${t.description}:id`), r);
  const o = J(t, null);
  if (!o) {
    if (!n) return o;
    throw new Error(`[Vuetify] Could not find useGroup injection with symbol ${t.description}`);
  }
  const a = j(e, "value"), s = g(() => !!(o.disabled.value || e.disabled));
  o.register({
    id: r,
    value: a,
    disabled: s
  }, i), Ce(() => {
    o.unregister(r);
  });
  const l = g(() => o.isSelected(r)), u = g(() => o.items.value[0].id === r), f = g(() => o.items.value[o.items.value.length - 1].id === r), v = g(() => l.value && [o.selectedClass.value, e.selectedClass]);
  return I(l, (c) => {
    i.emit("group:selected", {
      value: c
    });
  }, {
    flush: "sync"
  }), {
    id: r,
    isSelected: l,
    isFirst: u,
    isLast: f,
    toggle: () => o.select(r, !l.value),
    select: (c) => o.select(r, c),
    selectedClass: v,
    value: a,
    disabled: s,
    group: o
  };
}
function er(e, t) {
  let n = !1;
  const i = le([]), r = yt(e, "modelValue", [], (c) => c == null ? [] : En(i, hi(c)), (c) => {
    const m = nr(i, c);
    return e.multiple ? m : m[0];
  }), o = N("useGroup");
  function a(c, m) {
    const h = c, d = Symbol.for(`${t.description}:id`), w = he(d, o == null ? void 0 : o.vnode).indexOf(m);
    K(h.value) == null && (h.value = w, h.useIndexAsValue = !0), w > -1 ? i.splice(w, 0, h) : i.push(h);
  }
  function s(c) {
    if (n) return;
    l();
    const m = i.findIndex((h) => h.id === c);
    i.splice(m, 1);
  }
  function l() {
    const c = i.find((m) => !m.disabled);
    c && e.mandatory === "force" && !r.value.length && (r.value = [c.id]);
  }
  ct(() => {
    l();
  }), Ce(() => {
    n = !0;
  }), Xn(() => {
    for (let c = 0; c < i.length; c++)
      i[c].useIndexAsValue && (i[c].value = c);
  });
  function u(c, m) {
    const h = i.find((d) => d.id === c);
    if (!(m && (h != null && h.disabled)))
      if (e.multiple) {
        const d = r.value.slice(), b = d.findIndex((E) => E === c), w = ~b;
        if (m = m ?? !w, w && e.mandatory && d.length <= 1 || !w && e.max != null && d.length + 1 > e.max) return;
        b < 0 && m ? d.push(c) : b >= 0 && !m && d.splice(b, 1), r.value = d;
      } else {
        const d = r.value.includes(c);
        if (e.mandatory && d) return;
        r.value = m ?? !d ? [c] : [];
      }
  }
  function f(c) {
    if (e.multiple && se('This method is not supported when using "multiple" prop'), r.value.length) {
      const m = r.value[0], h = i.findIndex((w) => w.id === m);
      let d = (h + c) % i.length, b = i[d];
      for (; b.disabled && d !== h; )
        d = (d + c) % i.length, b = i[d];
      if (b.disabled) return;
      r.value = [i[d].id];
    } else {
      const m = i.find((h) => !h.disabled);
      m && (r.value = [m.id]);
    }
  }
  const v = {
    register: a,
    unregister: s,
    selected: r,
    select: u,
    disabled: j(e, "disabled"),
    prev: () => f(i.length - 1),
    next: () => f(1),
    isSelected: (c) => r.value.includes(c),
    selectedClass: g(() => e.selectedClass),
    items: g(() => i),
    getItemIndex: (c) => tr(i, c)
  };
  return fe(t, v), v;
}
function tr(e, t) {
  const n = En(e, [t]);
  return n.length ? e.findIndex((i) => i.id === n[0]) : -1;
}
function En(e, t) {
  const n = [];
  return t.forEach((i) => {
    const r = e.find((a) => ft(i, a.value)), o = e[i];
    (r == null ? void 0 : r.value) != null ? n.push(r.id) : o != null && n.push(o.id);
  }), n;
}
function nr(e, t) {
  const n = [];
  return t.forEach((i) => {
    const r = e.findIndex((o) => o.id === i);
    if (~r) {
      const o = e[r];
      n.push(o.value != null ? o.value : r);
    }
  }), n;
}
const Pn = Symbol.for("vuetify:v-btn-toggle"), ir = S({
  ...kn(),
  ...Ji()
}, "VBtnToggle");
Y()({
  name: "VBtnToggle",
  props: ir(),
  emits: {
    "update:modelValue": (e) => !0
  },
  setup(e, t) {
    let {
      slots: n
    } = t;
    const {
      isSelected: i,
      next: r,
      prev: o,
      select: a,
      selected: s
    } = er(e, Pn);
    return te(() => {
      const l = jt.filterProps(e);
      return p(jt, z({
        class: ["v-btn-toggle", e.class]
      }, l, {
        style: e.style
      }), {
        default: () => {
          var u;
          return [(u = n.default) == null ? void 0 : u.call(n, {
            isSelected: i,
            next: r,
            prev: o,
            select: a,
            selected: s
          })];
        }
      });
    }), {
      next: r,
      prev: o,
      select: a
    };
  }
});
const rr = S({
  defaults: Object,
  disabled: Boolean,
  reset: [Number, String],
  root: [Boolean, String],
  scoped: Boolean
}, "VDefaultsProvider"), qe = Y(!1)({
  name: "VDefaultsProvider",
  props: rr(),
  setup(e, t) {
    let {
      slots: n
    } = t;
    const {
      defaults: i,
      disabled: r,
      reset: o,
      root: a,
      scoped: s
    } = tn(e);
    return vn(i, {
      reset: o,
      root: a,
      scoped: s,
      disabled: r
    }), () => {
      var l;
      return (l = n.default) == null ? void 0 : l.call(n);
    };
  }
}), Ve = [String, Function, Object, Array], or = Symbol.for("vuetify:icons"), We = S({
  icon: {
    type: Ve
  },
  // Could not remove this and use makeTagProps, types complained because it is not required
  tag: {
    type: String,
    required: !0
  }
}, "icon"), Gt = Y()({
  name: "VComponentIcon",
  props: We(),
  setup(e, t) {
    let {
      slots: n
    } = t;
    return () => {
      const i = e.icon;
      return p(e.tag, null, {
        default: () => {
          var r;
          return [e.icon ? p(i, null, null) : (r = n.default) == null ? void 0 : r.call(n)];
        }
      });
    };
  }
}), ar = Me({
  name: "VSvgIcon",
  inheritAttrs: !1,
  props: We(),
  setup(e, t) {
    let {
      attrs: n
    } = t;
    return () => p(e.tag, z(n, {
      style: null
    }), {
      default: () => [p("svg", {
        class: "v-icon__svg",
        xmlns: "http://www.w3.org/2000/svg",
        viewBox: "0 0 24 24",
        role: "img",
        "aria-hidden": "true"
      }, [Array.isArray(e.icon) ? e.icon.map((i) => Array.isArray(i) ? p("path", {
        d: i[0],
        "fill-opacity": i[1]
      }, null) : p("path", {
        d: i
      }, null)) : p("path", {
        d: e.icon
      }, null)])]
    });
  }
});
Me({
  name: "VLigatureIcon",
  props: We(),
  setup(e) {
    return () => p(e.tag, null, {
      default: () => [e.icon]
    });
  }
});
Me({
  name: "VClassIcon",
  props: We(),
  setup(e) {
    return () => p(e.tag, {
      class: e.icon
    }, null);
  }
});
const sr = (e) => {
  const t = J(or);
  if (!t) throw new Error("Missing Vuetify Icons provide!");
  return {
    iconData: g(() => {
      var l;
      const i = K(e);
      if (!i) return {
        component: Gt
      };
      let r = i;
      if (typeof r == "string" && (r = r.trim(), r.startsWith("$") && (r = (l = t.aliases) == null ? void 0 : l[r.slice(1)])), r || se(`Could not find aliased icon "${i}"`), Array.isArray(r))
        return {
          component: ar,
          icon: r
        };
      if (typeof r != "string")
        return {
          component: Gt,
          icon: r
        };
      const o = Object.keys(t.sets).find((u) => typeof r == "string" && r.startsWith(`${u}:`)), a = o ? r.slice(o.length + 1) : r;
      return {
        component: t.sets[o ?? t.defaultSet].component,
        icon: a
      };
    })
  };
}, lr = ["x-small", "small", "default", "large", "x-large"], bt = S({
  size: {
    type: [String, Number],
    default: "default"
  }
}, "size");
function pt(e) {
  let t = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : Z();
  return dt(() => {
    let n, i;
    return Be(lr, e.size) ? n = `${t}--size-${e.size}` : e.size && (i = {
      width: L(e.size),
      height: L(e.size)
    }), {
      sizeClasses: n,
      sizeStyles: i
    };
  });
}
const ur = S({
  color: String,
  disabled: Boolean,
  start: Boolean,
  end: Boolean,
  icon: Ve,
  ...Se(),
  ...bt(),
  ...Fe({
    tag: "i"
  }),
  ...xe()
}, "VIcon"), _e = Y()({
  name: "VIcon",
  props: ur(),
  setup(e, t) {
    let {
      attrs: n,
      slots: i
    } = t;
    const r = B(), {
      themeClasses: o
    } = ke(e), {
      iconData: a
    } = sr(g(() => r.value || e.icon)), {
      sizeClasses: s
    } = pt(e), {
      textColorClasses: l,
      textColorStyles: u
    } = nt(j(e, "color"));
    return te(() => {
      var c, m;
      const f = (c = i.default) == null ? void 0 : c.call(i);
      f && (r.value = (m = ln(f).filter((h) => h.type === Kn && h.children && typeof h.children == "string")[0]) == null ? void 0 : m.children);
      const v = !!(n.onClick || n.onClickOnce);
      return p(a.value.component, {
        tag: e.tag,
        icon: a.value.icon,
        class: ["v-icon", "notranslate", o.value, s.value, l.value, {
          "v-icon--clickable": v,
          "v-icon--disabled": e.disabled,
          "v-icon--start": e.start,
          "v-icon--end": e.end
        }, e.class],
        style: [s.value ? void 0 : {
          fontSize: L(e.size),
          height: L(e.size),
          width: L(e.size)
        }, u.value, e.style],
        role: v ? "button" : void 0,
        "aria-hidden": !v,
        tabindex: v ? e.disabled ? -1 : 0 : void 0
      }, {
        default: () => [f]
      });
    }), {};
  }
});
function cr(e, t) {
  const n = B(), i = U(!1);
  if (ci) {
    const r = new IntersectionObserver((o) => {
      i.value = !!o.find((a) => a.isIntersecting);
    }, t);
    Ce(() => {
      r.disconnect();
    }), I(n, (o, a) => {
      a && (r.unobserve(a), i.value = !1), o && r.observe(o);
    }, {
      flush: "post"
    });
  }
  return {
    intersectionRef: n,
    isIntersecting: i
  };
}
function fr(e) {
  let t = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : "content";
  const n = Qe(), i = B();
  if (G) {
    const r = new ResizeObserver((o) => {
      o.length && (t === "content" ? i.value = o[0].contentRect : i.value = o[0].target.getBoundingClientRect());
    });
    Ce(() => {
      r.disconnect();
    }), I(() => n.el, (o, a) => {
      a && (r.unobserve(a), i.value = void 0), o && r.observe(o);
    }, {
      flush: "post"
    });
  }
  return {
    resizeRef: n,
    contentRect: rn(i)
  };
}
const dr = S({
  bgColor: String,
  color: String,
  indeterminate: [Boolean, String],
  modelValue: {
    type: [Number, String],
    default: 0
  },
  rotate: {
    type: [Number, String],
    default: 0
  },
  width: {
    type: [Number, String],
    default: 4
  },
  ...Se(),
  ...bt(),
  ...Fe({
    tag: "div"
  }),
  ...xe()
}, "VProgressCircular"), vr = Y()({
  name: "VProgressCircular",
  props: dr(),
  setup(e, t) {
    let {
      slots: n
    } = t;
    const i = 20, r = 2 * Math.PI * i, o = B(), {
      themeClasses: a
    } = ke(e), {
      sizeClasses: s,
      sizeStyles: l
    } = pt(e), {
      textColorClasses: u,
      textColorStyles: f
    } = nt(j(e, "color")), {
      textColorClasses: v,
      textColorStyles: c
    } = nt(j(e, "bgColor")), {
      intersectionRef: m,
      isIntersecting: h
    } = cr(), {
      resizeRef: d,
      contentRect: b
    } = fr(), w = g(() => Math.max(0, Math.min(100, parseFloat(e.modelValue)))), E = g(() => Number(e.width)), T = g(() => l.value ? Number(e.size) : b.value ? b.value.width : Math.max(E.value, 32)), A = g(() => i / (1 - E.value / T.value) * 2), R = g(() => E.value / T.value * A.value), V = g(() => L((100 - w.value) / 100 * r));
    return ce(() => {
      m.value = o.value, d.value = o.value;
    }), te(() => p(e.tag, {
      ref: o,
      class: ["v-progress-circular", {
        "v-progress-circular--indeterminate": !!e.indeterminate,
        "v-progress-circular--visible": h.value,
        "v-progress-circular--disable-shrink": e.indeterminate === "disable-shrink"
      }, a.value, s.value, u.value, e.class],
      style: [l.value, f.value, e.style],
      role: "progressbar",
      "aria-valuemin": "0",
      "aria-valuemax": "100",
      "aria-valuenow": e.indeterminate ? void 0 : w.value
    }, {
      default: () => [p("svg", {
        style: {
          transform: `rotate(calc(-90deg + ${Number(e.rotate)}deg))`
        },
        xmlns: "http://www.w3.org/2000/svg",
        viewBox: `0 0 ${A.value} ${A.value}`
      }, [p("circle", {
        class: ["v-progress-circular__underlay", v.value],
        style: c.value,
        fill: "transparent",
        cx: "50%",
        cy: "50%",
        r: i,
        "stroke-width": R.value,
        "stroke-dasharray": r,
        "stroke-dashoffset": 0
      }, null), p("circle", {
        class: "v-progress-circular__overlay",
        fill: "transparent",
        cx: "50%",
        cy: "50%",
        r: i,
        "stroke-width": R.value,
        "stroke-dasharray": r,
        "stroke-dashoffset": V.value
      }, null)]), n.default && p("div", {
        class: "v-progress-circular__content"
      }, [n.default({
        value: w.value
      })])]
    })), {};
  }
}), On = S({
  height: [Number, String],
  maxHeight: [Number, String],
  maxWidth: [Number, String],
  minHeight: [Number, String],
  minWidth: [Number, String],
  width: [Number, String]
}, "dimension");
function Tn(e) {
  return {
    dimensionStyles: g(() => {
      const n = {}, i = L(e.height), r = L(e.maxHeight), o = L(e.maxWidth), a = L(e.minHeight), s = L(e.minWidth), l = L(e.width);
      return i != null && (n.height = i), r != null && (n.maxHeight = r), o != null && (n.maxWidth = o), a != null && (n.minHeight = a), s != null && (n.minWidth = s), l != null && (n.width = l), n;
    })
  };
}
const mr = Symbol.for("vuetify:locale");
function Ln() {
  const e = J(mr);
  if (!e) throw new Error("[Vuetify] Could not find injected rtl instance");
  return {
    isRtl: e.isRtl,
    rtlClasses: e.rtlClasses
  };
}
const qt = {
  center: "center",
  top: "bottom",
  bottom: "top",
  left: "right",
  right: "left"
}, gr = S({
  location: String
}, "location");
function hr(e) {
  let t = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : !1, n = arguments.length > 2 ? arguments[2] : void 0;
  const {
    isRtl: i
  } = Ln();
  return {
    locationStyles: g(() => {
      if (!e.location) return {};
      const {
        side: o,
        align: a
      } = et(e.location.split(" ").length > 1 ? e.location : `${e.location} center`, i.value);
      function s(u) {
        return n ? n(u) : 0;
      }
      const l = {};
      return o !== "center" && (t ? l[qt[o]] = `calc(100% - ${s(o)}px)` : l[o] = 0), a !== "center" ? t ? l[qt[a]] = `calc(100% - ${s(a)}px)` : l[a] = 0 : (o === "center" ? l.top = l.left = "50%" : l[{
        top: "left",
        bottom: "left",
        left: "top",
        right: "top"
      }[o]] = "50%", l.transform = {
        top: "translateX(-50%)",
        bottom: "translateX(-50%)",
        left: "translateY(-50%)",
        right: "translateY(-50%)",
        center: "translate(-50%, -50%)"
      }[o]), l;
    })
  };
}
const yr = S({
  loading: [Boolean, String]
}, "loader");
function br(e) {
  let t = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : Z();
  return {
    loaderClasses: g(() => ({
      [`${t}--loading`]: e.loading
    }))
  };
}
const pr = ["static", "relative", "fixed", "absolute", "sticky"], wr = S({
  position: {
    type: String,
    validator: (
      /* istanbul ignore next */
      (e) => pr.includes(e)
    )
  }
}, "position");
function Cr(e) {
  let t = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : Z();
  return {
    positionClasses: g(() => e.position ? `${t}--${e.position}` : void 0)
  };
}
function Sr() {
  const e = N("useRoute");
  return g(() => {
    var t;
    return (t = e == null ? void 0 : e.proxy) == null ? void 0 : t.$route;
  });
}
function xr() {
  var e, t;
  return (t = (e = N("useRouter")) == null ? void 0 : e.proxy) == null ? void 0 : t.$router;
}
function kr(e, t) {
  var v, c;
  const n = Jn("RouterLink"), i = g(() => !!(e.href || e.to)), r = g(() => (i == null ? void 0 : i.value) || Tt(t, "click") || Tt(e, "click"));
  if (typeof n == "string" || !("useLink" in n)) {
    const m = j(e, "href");
    return {
      isLink: i,
      isClickable: r,
      href: m,
      linkProps: le({
        href: m
      })
    };
  }
  const o = g(() => ({
    ...e,
    to: j(() => e.to || "")
  })), a = n.useLink(o.value), s = g(() => e.to ? a : void 0), l = Sr(), u = g(() => {
    var m, h, d;
    return s.value ? e.exact ? l.value ? ((d = s.value.isExactActive) == null ? void 0 : d.value) && ft(s.value.route.value.query, l.value.query) : ((h = s.value.isExactActive) == null ? void 0 : h.value) ?? !1 : ((m = s.value.isActive) == null ? void 0 : m.value) ?? !1 : !1;
  }), f = g(() => {
    var m;
    return e.to ? (m = s.value) == null ? void 0 : m.route.value.href : e.href;
  });
  return {
    isLink: i,
    isClickable: r,
    isActive: u,
    route: (v = s.value) == null ? void 0 : v.route,
    navigate: (c = s.value) == null ? void 0 : c.navigate,
    href: f,
    linkProps: le({
      href: f,
      "aria-current": g(() => u.value ? "page" : void 0)
    })
  };
}
const Er = S({
  href: String,
  replace: Boolean,
  to: [String, Object],
  exact: Boolean
}, "router");
let Ue = !1;
function Pr(e, t) {
  let n = !1, i, r;
  G && (de(() => {
    window.addEventListener("popstate", o), i = e == null ? void 0 : e.beforeEach((a, s, l) => {
      Ue ? n ? t(l) : l() : setTimeout(() => n ? t(l) : l()), Ue = !0;
    }), r = e == null ? void 0 : e.afterEach(() => {
      Ue = !1;
    });
  }), q(() => {
    window.removeEventListener("popstate", o), i == null || i(), r == null || r();
  }));
  function o(a) {
    var s;
    (s = a.state) != null && s.replaced || (n = !0, setTimeout(() => n = !1));
  }
}
function Or(e, t) {
  I(() => {
    var n;
    return (n = e.isActive) == null ? void 0 : n.value;
  }, (n) => {
    e.isLink.value && n && t && de(() => {
      t(!0);
    });
  }, {
    immediate: !0
  });
}
const it = Symbol("rippleStop"), Tr = 80;
function Ut(e, t) {
  e.style.transform = t, e.style.webkitTransform = t;
}
function rt(e) {
  return e.constructor.name === "TouchEvent";
}
function _n(e) {
  return e.constructor.name === "KeyboardEvent";
}
const Lr = function(e, t) {
  var v;
  let n = arguments.length > 2 && arguments[2] !== void 0 ? arguments[2] : {}, i = 0, r = 0;
  if (!_n(e)) {
    const c = t.getBoundingClientRect(), m = rt(e) ? e.touches[e.touches.length - 1] : e;
    i = m.clientX - c.left, r = m.clientY - c.top;
  }
  let o = 0, a = 0.3;
  (v = t._ripple) != null && v.circle ? (a = 0.15, o = t.clientWidth / 2, o = n.center ? o : o + Math.sqrt((i - o) ** 2 + (r - o) ** 2) / 4) : o = Math.sqrt(t.clientWidth ** 2 + t.clientHeight ** 2) / 2;
  const s = `${(t.clientWidth - o * 2) / 2}px`, l = `${(t.clientHeight - o * 2) / 2}px`, u = n.center ? s : `${i - o}px`, f = n.center ? l : `${r - o}px`;
  return {
    radius: o,
    scale: a,
    x: u,
    y: f,
    centerX: s,
    centerY: l
  };
}, $e = {
  /* eslint-disable max-statements */
  show(e, t) {
    var m;
    let n = arguments.length > 2 && arguments[2] !== void 0 ? arguments[2] : {};
    if (!((m = t == null ? void 0 : t._ripple) != null && m.enabled))
      return;
    const i = document.createElement("span"), r = document.createElement("span");
    i.appendChild(r), i.className = "v-ripple__container", n.class && (i.className += ` ${n.class}`);
    const {
      radius: o,
      scale: a,
      x: s,
      y: l,
      centerX: u,
      centerY: f
    } = Lr(e, t, n), v = `${o * 2}px`;
    r.className = "v-ripple__animation", r.style.width = v, r.style.height = v, t.appendChild(i);
    const c = window.getComputedStyle(t);
    c && c.position === "static" && (t.style.position = "relative", t.dataset.previousPosition = "static"), r.classList.add("v-ripple__animation--enter"), r.classList.add("v-ripple__animation--visible"), Ut(r, `translate(${s}, ${l}) scale3d(${a},${a},${a})`), r.dataset.activated = String(performance.now()), setTimeout(() => {
      r.classList.remove("v-ripple__animation--enter"), r.classList.add("v-ripple__animation--in"), Ut(r, `translate(${u}, ${f}) scale3d(1,1,1)`);
    }, 0);
  },
  hide(e) {
    var o;
    if (!((o = e == null ? void 0 : e._ripple) != null && o.enabled)) return;
    const t = e.getElementsByClassName("v-ripple__animation");
    if (t.length === 0) return;
    const n = t[t.length - 1];
    if (n.dataset.isHiding) return;
    n.dataset.isHiding = "true";
    const i = performance.now() - Number(n.dataset.activated), r = Math.max(250 - i, 0);
    setTimeout(() => {
      n.classList.remove("v-ripple__animation--in"), n.classList.add("v-ripple__animation--out"), setTimeout(() => {
        var s;
        e.getElementsByClassName("v-ripple__animation").length === 1 && e.dataset.previousPosition && (e.style.position = e.dataset.previousPosition, delete e.dataset.previousPosition), ((s = n.parentNode) == null ? void 0 : s.parentNode) === e && e.removeChild(n.parentNode);
      }, 300);
    }, r);
  }
};
function An(e) {
  return typeof e > "u" || !!e;
}
function pe(e) {
  const t = {}, n = e.currentTarget;
  if (!(!(n != null && n._ripple) || n._ripple.touched || e[it])) {
    if (e[it] = !0, rt(e))
      n._ripple.touched = !0, n._ripple.isTouch = !0;
    else if (n._ripple.isTouch) return;
    if (t.center = n._ripple.centered || _n(e), n._ripple.class && (t.class = n._ripple.class), rt(e)) {
      if (n._ripple.showTimerCommit) return;
      n._ripple.showTimerCommit = () => {
        $e.show(e, n, t);
      }, n._ripple.showTimer = window.setTimeout(() => {
        var i;
        (i = n == null ? void 0 : n._ripple) != null && i.showTimerCommit && (n._ripple.showTimerCommit(), n._ripple.showTimerCommit = null);
      }, Tr);
    } else
      $e.show(e, n, t);
  }
}
function Yt(e) {
  e[it] = !0;
}
function W(e) {
  const t = e.currentTarget;
  if (t != null && t._ripple) {
    if (window.clearTimeout(t._ripple.showTimer), e.type === "touchend" && t._ripple.showTimerCommit) {
      t._ripple.showTimerCommit(), t._ripple.showTimerCommit = null, t._ripple.showTimer = window.setTimeout(() => {
        W(e);
      });
      return;
    }
    window.setTimeout(() => {
      t._ripple && (t._ripple.touched = !1);
    }), $e.hide(t);
  }
}
function Bn(e) {
  const t = e.currentTarget;
  t != null && t._ripple && (t._ripple.showTimerCommit && (t._ripple.showTimerCommit = null), window.clearTimeout(t._ripple.showTimer));
}
let we = !1;
function In(e) {
  !we && (e.keyCode === Et.enter || e.keyCode === Et.space) && (we = !0, pe(e));
}
function Rn(e) {
  we = !1, W(e);
}
function Vn(e) {
  we && (we = !1, W(e));
}
function $n(e, t, n) {
  const {
    value: i,
    modifiers: r
  } = t, o = An(i);
  if (o || $e.hide(e), e._ripple = e._ripple ?? {}, e._ripple.enabled = o, e._ripple.centered = r.center, e._ripple.circle = r.circle, fi(i) && i.class && (e._ripple.class = i.class), o && !n) {
    if (r.stop) {
      e.addEventListener("touchstart", Yt, {
        passive: !0
      }), e.addEventListener("mousedown", Yt);
      return;
    }
    e.addEventListener("touchstart", pe, {
      passive: !0
    }), e.addEventListener("touchend", W, {
      passive: !0
    }), e.addEventListener("touchmove", Bn, {
      passive: !0
    }), e.addEventListener("touchcancel", W), e.addEventListener("mousedown", pe), e.addEventListener("mouseup", W), e.addEventListener("mouseleave", W), e.addEventListener("keydown", In), e.addEventListener("keyup", Rn), e.addEventListener("blur", Vn), e.addEventListener("dragstart", W, {
      passive: !0
    });
  } else !o && n && Nn(e);
}
function Nn(e) {
  e.removeEventListener("mousedown", pe), e.removeEventListener("touchstart", pe), e.removeEventListener("touchend", W), e.removeEventListener("touchmove", Bn), e.removeEventListener("touchcancel", W), e.removeEventListener("mouseup", W), e.removeEventListener("mouseleave", W), e.removeEventListener("keydown", In), e.removeEventListener("keyup", Rn), e.removeEventListener("dragstart", W), e.removeEventListener("blur", Vn);
}
function _r(e, t) {
  $n(e, t, !1);
}
function Ar(e) {
  delete e._ripple, Nn(e);
}
function Br(e, t) {
  if (t.value === t.oldValue)
    return;
  const n = An(t.oldValue);
  $n(e, t, n);
}
const Ir = {
  mounted: _r,
  unmounted: Ar,
  updated: Br
}, Rr = S({
  active: {
    type: Boolean,
    default: void 0
  },
  activeColor: String,
  baseColor: String,
  symbol: {
    type: null,
    default: Pn
  },
  flat: Boolean,
  icon: [Boolean, String, Function, Object],
  prependIcon: Ve,
  appendIcon: Ve,
  block: Boolean,
  readonly: Boolean,
  slim: Boolean,
  stacked: Boolean,
  ripple: {
    type: [Boolean, Object],
    default: !0
  },
  text: String,
  ...gn(),
  ...Se(),
  ...yn(),
  ...On(),
  ...pn(),
  ...Zi(),
  ...yr(),
  ...gr(),
  ...wr(),
  ...Cn(),
  ...Er(),
  ...bt(),
  ...Fe({
    tag: "button"
  }),
  ...xe(),
  ...xn({
    variant: "elevated"
  })
}, "VBtn"), Vr = Y()({
  name: "VBtn",
  props: Rr(),
  emits: {
    "group:selected": (e) => !0
  },
  setup(e, t) {
    let {
      attrs: n,
      slots: i
    } = t;
    const {
      themeClasses: r
    } = ke(e), {
      borderClasses: o
    } = hn(e), {
      densityClasses: a
    } = bn(e), {
      dimensionStyles: s
    } = Tn(e), {
      elevationClasses: l
    } = wn(e), {
      loaderClasses: u
    } = br(e), {
      locationStyles: f
    } = hr(e), {
      positionClasses: v
    } = Cr(e), {
      roundedClasses: c
    } = Sn(e), {
      sizeClasses: m,
      sizeStyles: h
    } = pt(e), d = Qi(e, e.symbol, !1), b = kr(e, n), w = g(() => {
      var x;
      return e.active !== void 0 ? e.active : b.isLink.value ? (x = b.isActive) == null ? void 0 : x.value : d == null ? void 0 : d.isSelected.value;
    }), E = g(() => w.value ? e.activeColor ?? e.color : e.color), T = g(() => {
      var k, C;
      return {
        color: (d == null ? void 0 : d.isSelected.value) && (!b.isLink.value || ((k = b.isActive) == null ? void 0 : k.value)) || !d || ((C = b.isActive) == null ? void 0 : C.value) ? E.value ?? e.baseColor : e.baseColor,
        variant: e.variant
      };
    }), {
      colorClasses: A,
      colorStyles: R,
      variantClasses: V
    } = Ki(T), _ = g(() => (d == null ? void 0 : d.disabled.value) || e.disabled), y = g(() => e.variant === "elevated" && !(e.disabled || e.flat || e.border)), $ = g(() => {
      if (!(e.value === void 0 || typeof e.value == "symbol"))
        return Object(e.value) === e.value ? JSON.stringify(e.value, null, 0) : e.value;
    });
    function ve(x) {
      var k;
      _.value || b.isLink.value && (x.metaKey || x.ctrlKey || x.shiftKey || x.button !== 0 || n.target === "_blank") || ((k = b.navigate) == null || k.call(b, x), d == null || d.toggle());
    }
    return Or(b, d == null ? void 0 : d.select), te(() => {
      const x = b.isLink.value ? "a" : e.tag, k = !!(e.prependIcon || i.prepend), C = !!(e.appendIcon || i.append), P = !!(e.icon && e.icon !== !0);
      return on(p(x, z({
        type: x === "a" ? void 0 : "button",
        class: ["v-btn", d == null ? void 0 : d.selectedClass.value, {
          "v-btn--active": w.value,
          "v-btn--block": e.block,
          "v-btn--disabled": _.value,
          "v-btn--elevated": y.value,
          "v-btn--flat": e.flat,
          "v-btn--icon": !!e.icon,
          "v-btn--loading": e.loading,
          "v-btn--readonly": e.readonly,
          "v-btn--slim": e.slim,
          "v-btn--stacked": e.stacked
        }, r.value, o.value, A.value, a.value, l.value, u.value, v.value, c.value, m.value, V.value, e.class],
        style: [R.value, s.value, f.value, h.value, e.style],
        "aria-busy": e.loading ? !0 : void 0,
        disabled: _.value || void 0,
        tabindex: e.loading || e.readonly ? -1 : void 0,
        onClick: ve,
        value: $.value
      }, b.linkProps), {
        default: () => {
          var F;
          return [Xi(!0, "v-btn"), !e.icon && k && p("span", {
            key: "prepend",
            class: "v-btn__prepend"
          }, [i.prepend ? p(qe, {
            key: "prepend-defaults",
            disabled: !e.prependIcon,
            defaults: {
              VIcon: {
                icon: e.prependIcon
              }
            }
          }, i.prepend) : p(_e, {
            key: "prepend-icon",
            icon: e.prependIcon
          }, null)]), p("span", {
            class: "v-btn__content",
            "data-no-activator": ""
          }, [!i.default && P ? p(_e, {
            key: "content-icon",
            icon: e.icon
          }, null) : p(qe, {
            key: "content-defaults",
            disabled: !P,
            defaults: {
              VIcon: {
                icon: e.icon
              }
            }
          }, {
            default: () => {
              var D;
              return [((D = i.default) == null ? void 0 : D.call(i)) ?? e.text];
            }
          })]), !e.icon && C && p("span", {
            key: "append",
            class: "v-btn__append"
          }, [i.append ? p(qe, {
            key: "append-defaults",
            disabled: !e.appendIcon,
            defaults: {
              VIcon: {
                icon: e.appendIcon
              }
            }
          }, i.append) : p(_e, {
            key: "append-icon",
            icon: e.appendIcon
          }, null)]), !!e.loading && p("span", {
            key: "loader",
            class: "v-btn__loader"
          }, [((F = i.loader) == null ? void 0 : F.call(i)) ?? p(vr, {
            color: typeof e.loading == "boolean" ? void 0 : e.loading,
            indeterminate: !0,
            width: "2"
          }, null)])];
        }
      }), [[Ir, !_.value && e.ripple, "", {
        center: !!e.icon
      }]]);
    }), {
      group: d
    };
  }
});
function Ye(e, t) {
  return {
    x: e.x + t.x,
    y: e.y + t.y
  };
}
function $r(e, t) {
  return {
    x: e.x - t.x,
    y: e.y - t.y
  };
}
function Xt(e, t) {
  if (e.side === "top" || e.side === "bottom") {
    const {
      side: n,
      align: i
    } = e, r = i === "left" ? 0 : i === "center" ? t.width / 2 : i === "right" ? t.width : i, o = n === "top" ? 0 : n === "bottom" ? t.height : n;
    return Ye({
      x: r,
      y: o
    }, t);
  } else if (e.side === "left" || e.side === "right") {
    const {
      side: n,
      align: i
    } = e, r = n === "left" ? 0 : n === "right" ? t.width : n, o = i === "top" ? 0 : i === "center" ? t.height / 2 : i === "bottom" ? t.height : i;
    return Ye({
      x: r,
      y: o
    }, t);
  }
  return Ye({
    x: t.width / 2,
    y: t.height / 2
  }, t);
}
const Mn = {
  static: Fr,
  // specific viewport position, usually centered
  connected: Wr
  // connected to a certain element
}, Nr = S({
  locationStrategy: {
    type: [String, Function],
    default: "static",
    validator: (e) => typeof e == "function" || e in Mn
  },
  location: {
    type: String,
    default: "bottom"
  },
  origin: {
    type: String,
    default: "auto"
  },
  offset: [Number, String, Array]
}, "VOverlay-location-strategies");
function Mr(e, t) {
  const n = B({}), i = B();
  G && De(() => !!(t.isActive.value && e.locationStrategy), (o) => {
    var a, s;
    I(() => e.locationStrategy, o), q(() => {
      window.removeEventListener("resize", r), i.value = void 0;
    }), window.addEventListener("resize", r, {
      passive: !0
    }), typeof e.locationStrategy == "function" ? i.value = (a = e.locationStrategy(t, e, n)) == null ? void 0 : a.updateLocation : i.value = (s = Mn[e.locationStrategy](t, e, n)) == null ? void 0 : s.updateLocation;
  });
  function r(o) {
    var a;
    (a = i.value) == null || a.call(i, o);
  }
  return {
    contentStyles: n,
    updateLocation: i
  };
}
function Fr() {
}
function Dr(e, t) {
  const n = Si(e);
  return t ? n.x += parseFloat(e.style.right || 0) : n.x -= parseFloat(e.style.left || 0), n.y -= parseFloat(e.style.top || 0), n;
}
function Wr(e, t, n) {
  (Array.isArray(e.target.value) || Gi(e.target.value)) && Object.assign(n.value, {
    position: "fixed",
    top: 0,
    [e.isRtl.value ? "right" : "left"]: 0
  });
  const {
    preferredAnchor: r,
    preferredOrigin: o
  } = dt(() => {
    const h = et(t.location, e.isRtl.value), d = t.origin === "overlap" ? h : t.origin === "auto" ? je(h) : et(t.origin, e.isRtl.value);
    return h.side === d.side && h.align === Ge(d).align ? {
      preferredAnchor: _t(h),
      preferredOrigin: _t(d)
    } : {
      preferredAnchor: h,
      preferredOrigin: d
    };
  }), [a, s, l, u] = ["minWidth", "minHeight", "maxWidth", "maxHeight"].map((h) => g(() => {
    const d = parseFloat(t[h]);
    return isNaN(d) ? 1 / 0 : d;
  })), f = g(() => {
    if (Array.isArray(t.offset))
      return t.offset;
    if (typeof t.offset == "string") {
      const h = t.offset.split(" ").map(parseFloat);
      return h.length < 2 && h.push(0), h;
    }
    return typeof t.offset == "number" ? [t.offset, 0] : [0, 0];
  });
  let v = !1;
  const c = new ResizeObserver(() => {
    v && m();
  });
  I([e.target, e.contentEl], (h, d) => {
    let [b, w] = h, [E, T] = d;
    E && !Array.isArray(E) && c.unobserve(E), b && !Array.isArray(b) && c.observe(b), T && c.unobserve(T), w && c.observe(w);
  }, {
    immediate: !0
  }), q(() => {
    c.disconnect();
  });
  function m() {
    if (v = !1, requestAnimationFrame(() => v = !0), !e.target.value || !e.contentEl.value) return;
    const h = Ci(e.target.value), d = Dr(e.contentEl.value, e.isRtl.value), b = Re(e.contentEl.value), w = 12;
    b.length || (b.push(document.documentElement), e.contentEl.value.style.top && e.contentEl.value.style.left || (d.x -= parseFloat(document.documentElement.style.getPropertyValue("--v-body-scroll-x") || 0), d.y -= parseFloat(document.documentElement.style.getPropertyValue("--v-body-scroll-y") || 0)));
    const E = b.reduce((x, k) => {
      const C = k.getBoundingClientRect(), P = new ee({
        x: k === document.documentElement ? 0 : C.x,
        y: k === document.documentElement ? 0 : C.y,
        width: k.clientWidth,
        height: k.clientHeight
      });
      return x ? new ee({
        x: Math.max(x.left, P.left),
        y: Math.max(x.top, P.top),
        width: Math.min(x.right, P.right) - Math.max(x.left, P.left),
        height: Math.min(x.bottom, P.bottom) - Math.max(x.top, P.top)
      }) : P;
    }, void 0);
    E.x += w, E.y += w, E.width -= w * 2, E.height -= w * 2;
    let T = {
      anchor: r.value,
      origin: o.value
    };
    function A(x) {
      const k = new ee(d), C = Xt(x.anchor, h), P = Xt(x.origin, k);
      let {
        x: F,
        y: D
      } = $r(C, P);
      switch (x.anchor.side) {
        case "top":
          D -= f.value[0];
          break;
        case "bottom":
          D += f.value[0];
          break;
        case "left":
          F -= f.value[0];
          break;
        case "right":
          F += f.value[0];
          break;
      }
      switch (x.anchor.align) {
        case "top":
          D -= f.value[1];
          break;
        case "bottom":
          D += f.value[1];
          break;
        case "left":
          F -= f.value[1];
          break;
        case "right":
          F += f.value[1];
          break;
      }
      return k.x += F, k.y += D, k.width = Math.min(k.width, l.value), k.height = Math.min(k.height, u.value), {
        overflows: Bt(k, E),
        x: F,
        y: D
      };
    }
    let R = 0, V = 0;
    const _ = {
      x: 0,
      y: 0
    }, y = {
      x: !1,
      y: !1
    };
    let $ = -1;
    for (; ; ) {
      if ($++ > 10) {
        Ii("Infinite loop detected in connectedLocationStrategy");
        break;
      }
      const {
        x,
        y: k,
        overflows: C
      } = A(T);
      R += x, V += k, d.x += x, d.y += k;
      {
        const P = At(T.anchor), F = C.x.before || C.x.after, D = C.y.before || C.y.after;
        let ne = !1;
        if (["x", "y"].forEach((M) => {
          if (M === "x" && F && !y.x || M === "y" && D && !y.y) {
            const X = {
              anchor: {
                ...T.anchor
              },
              origin: {
                ...T.origin
              }
            }, ie = M === "x" ? P === "y" ? Ge : je : P === "y" ? je : Ge;
            X.anchor = ie(X.anchor), X.origin = ie(X.origin);
            const {
              overflows: re
            } = A(X);
            (re[M].before <= C[M].before && re[M].after <= C[M].after || re[M].before + re[M].after < (C[M].before + C[M].after) / 2) && (T = X, ne = y[M] = !0);
          }
        }), ne) continue;
      }
      C.x.before && (R += C.x.before, d.x += C.x.before), C.x.after && (R -= C.x.after, d.x -= C.x.after), C.y.before && (V += C.y.before, d.y += C.y.before), C.y.after && (V -= C.y.after, d.y -= C.y.after);
      {
        const P = Bt(d, E);
        _.x = E.width - P.x.before - P.x.after, _.y = E.height - P.y.before - P.y.after, R += P.x.before, d.x += P.x.before, V += P.y.before, d.y += P.y.before;
      }
      break;
    }
    const ve = At(T.anchor);
    return Object.assign(n.value, {
      "--v-overlay-anchor-origin": `${T.anchor.side} ${T.anchor.align}`,
      transformOrigin: `${T.origin.side} ${T.origin.align}`,
      // transform: `translate(${pixelRound(x)}px, ${pixelRound(y)}px)`,
      top: L(Xe(V)),
      left: e.isRtl.value ? void 0 : L(Xe(R)),
      right: e.isRtl.value ? L(Xe(-R)) : void 0,
      minWidth: L(ve === "y" ? Math.min(a.value, h.width) : a.value),
      maxWidth: L(Kt(Pt(_.x, a.value === 1 / 0 ? 0 : a.value, l.value))),
      maxHeight: L(Kt(Pt(_.y, s.value === 1 / 0 ? 0 : s.value, u.value)))
    }), {
      available: _,
      contentBox: d
    };
  }
  return I(() => [r.value, o.value, t.offset, t.minWidth, t.minHeight, t.maxWidth, t.maxHeight], () => m()), de(() => {
    const h = m();
    if (!h) return;
    const {
      available: d,
      contentBox: b
    } = h;
    b.height > d.y && requestAnimationFrame(() => {
      m(), requestAnimationFrame(() => {
        m();
      });
    });
  }), {
    updateLocation: m
  };
}
function Xe(e) {
  return Math.round(e * devicePixelRatio) / devicePixelRatio;
}
function Kt(e) {
  return Math.ceil(e * devicePixelRatio) / devicePixelRatio;
}
let ot = !0;
const Ne = [];
function zr(e) {
  !ot || Ne.length ? (Ne.push(e), at()) : (ot = !1, e(), at());
}
let Jt = -1;
function at() {
  cancelAnimationFrame(Jt), Jt = requestAnimationFrame(() => {
    const e = Ne.shift();
    e && e(), Ne.length ? at() : ot = !0;
  });
}
const Ae = {
  none: null,
  close: Gr,
  block: qr,
  reposition: Ur
}, Hr = S({
  scrollStrategy: {
    type: [String, Function],
    default: "block",
    validator: (e) => typeof e == "function" || e in Ae
  }
}, "VOverlay-scroll-strategies");
function jr(e, t) {
  if (!G) return;
  let n;
  ce(async () => {
    n == null || n.stop(), t.isActive.value && e.scrollStrategy && (n = ut(), await new Promise((i) => setTimeout(i)), n.active && n.run(() => {
      var i;
      typeof e.scrollStrategy == "function" ? e.scrollStrategy(t, e, n) : (i = Ae[e.scrollStrategy]) == null || i.call(Ae, t, e, n);
    }));
  }), q(() => {
    n == null || n.stop();
  });
}
function Gr(e) {
  function t(n) {
    e.isActive.value = !1;
  }
  Fn(e.targetEl.value ?? e.contentEl.value, t);
}
function qr(e, t) {
  var a;
  const n = (a = e.root.value) == null ? void 0 : a.offsetParent, i = [.../* @__PURE__ */ new Set([...Re(e.targetEl.value, t.contained ? n : void 0), ...Re(e.contentEl.value, t.contained ? n : void 0)])].filter((s) => !s.classList.contains("v-overlay-scroll-blocked")), r = window.innerWidth - document.documentElement.offsetWidth, o = ((s) => gt(s) && s)(n || document.documentElement);
  o && e.root.value.classList.add("v-overlay--scroll-blocked"), i.forEach((s, l) => {
    s.style.setProperty("--v-body-scroll-x", L(-s.scrollLeft)), s.style.setProperty("--v-body-scroll-y", L(-s.scrollTop)), s !== document.documentElement && s.style.setProperty("--v-scrollbar-offset", L(r)), s.classList.add("v-overlay-scroll-blocked");
  }), q(() => {
    i.forEach((s, l) => {
      const u = parseFloat(s.style.getPropertyValue("--v-body-scroll-x")), f = parseFloat(s.style.getPropertyValue("--v-body-scroll-y")), v = s.style.scrollBehavior;
      s.style.scrollBehavior = "auto", s.style.removeProperty("--v-body-scroll-x"), s.style.removeProperty("--v-body-scroll-y"), s.style.removeProperty("--v-scrollbar-offset"), s.classList.remove("v-overlay-scroll-blocked"), s.scrollLeft = -u, s.scrollTop = -f, s.style.scrollBehavior = v;
    }), o && e.root.value.classList.remove("v-overlay--scroll-blocked");
  });
}
function Ur(e, t, n) {
  let i = !1, r = -1, o = -1;
  function a(s) {
    zr(() => {
      var f, v;
      const l = performance.now();
      (v = (f = e.updateLocation).value) == null || v.call(f, s), i = (performance.now() - l) / (1e3 / 60) > 2;
    });
  }
  o = (typeof requestIdleCallback > "u" ? (s) => s() : requestIdleCallback)(() => {
    n.run(() => {
      Fn(e.targetEl.value ?? e.contentEl.value, (s) => {
        i ? (cancelAnimationFrame(r), r = requestAnimationFrame(() => {
          r = requestAnimationFrame(() => {
            a(s);
          });
        })) : a(s);
      });
    });
  }), q(() => {
    typeof cancelIdleCallback < "u" && cancelIdleCallback(o), cancelAnimationFrame(r);
  });
}
function Fn(e, t) {
  const n = [document, ...Re(e)];
  n.forEach((i) => {
    i.addEventListener("scroll", t, {
      passive: !0
    });
  }), q(() => {
    n.forEach((i) => {
      i.removeEventListener("scroll", t);
    });
  });
}
const Yr = Symbol.for("vuetify:v-menu"), Xr = S({
  closeDelay: [Number, String],
  openDelay: [Number, String]
}, "delay");
function Kr(e, t) {
  let n = () => {
  };
  function i(a) {
    n == null || n();
    const s = Number(a ? e.openDelay : e.closeDelay);
    return new Promise((l) => {
      n = pi(s, () => {
        t == null || t(a), l(a);
      });
    });
  }
  function r() {
    return i(!0);
  }
  function o() {
    return i(!1);
  }
  return {
    clearDelay: n,
    runOpenDelay: r,
    runCloseDelay: o
  };
}
const Jr = S({
  target: [String, Object],
  activator: [String, Object],
  activatorProps: {
    type: Object,
    default: () => ({})
  },
  openOnClick: {
    type: Boolean,
    default: void 0
  },
  openOnHover: Boolean,
  openOnFocus: {
    type: Boolean,
    default: void 0
  },
  closeOnContentClick: Boolean,
  ...Xr()
}, "VOverlay-activator");
function Zr(e, t) {
  let {
    isActive: n,
    isTop: i,
    contentEl: r
  } = t;
  const o = N("useActivator"), a = B();
  let s = !1, l = !1, u = !0;
  const f = g(() => e.openOnFocus || e.openOnFocus == null && e.openOnHover), v = g(() => e.openOnClick || e.openOnClick == null && !e.openOnHover && !f.value), {
    runOpenDelay: c,
    runCloseDelay: m
  } = Kr(e, (y) => {
    y === (e.openOnHover && s || f.value && l) && !(e.openOnHover && n.value && !i.value) && (n.value !== y && (u = !0), n.value = y);
  }), h = B(), d = {
    onClick: (y) => {
      y.stopPropagation(), a.value = y.currentTarget || y.target, n.value || (h.value = [y.clientX, y.clientY]), n.value = !n.value;
    },
    onMouseenter: (y) => {
      var $;
      ($ = y.sourceCapabilities) != null && $.firesTouchEvents || (s = !0, a.value = y.currentTarget || y.target, c());
    },
    onMouseleave: (y) => {
      s = !1, m();
    },
    onFocus: (y) => {
      bi(y.target, ":focus-visible") !== !1 && (l = !0, y.stopPropagation(), a.value = y.currentTarget || y.target, c());
    },
    onBlur: (y) => {
      l = !1, y.stopPropagation(), m();
    }
  }, b = g(() => {
    const y = {};
    return v.value && (y.onClick = d.onClick), e.openOnHover && (y.onMouseenter = d.onMouseenter, y.onMouseleave = d.onMouseleave), f.value && (y.onFocus = d.onFocus, y.onBlur = d.onBlur), y;
  }), w = g(() => {
    const y = {};
    if (e.openOnHover && (y.onMouseenter = () => {
      s = !0, c();
    }, y.onMouseleave = () => {
      s = !1, m();
    }), f.value && (y.onFocusin = () => {
      l = !0, c();
    }, y.onFocusout = () => {
      l = !1, m();
    }), e.closeOnContentClick) {
      const $ = J(Yr, null);
      y.onClick = () => {
        n.value = !1, $ == null || $.closeParents();
      };
    }
    return y;
  }), E = g(() => {
    const y = {};
    return e.openOnHover && (y.onMouseenter = () => {
      u && (s = !0, u = !1, c());
    }, y.onMouseleave = () => {
      s = !1, m();
    }), y;
  });
  I(i, (y) => {
    var $;
    y && (e.openOnHover && !s && (!f.value || !l) || f.value && !l && (!e.openOnHover || !s)) && !(($ = r.value) != null && $.contains(document.activeElement)) && (n.value = !1);
  }), I(n, (y) => {
    y || setTimeout(() => {
      h.value = void 0;
    });
  }, {
    flush: "post"
  });
  const T = Qe();
  ce(() => {
    T.value && de(() => {
      a.value = T.el;
    });
  });
  const A = Qe(), R = g(() => e.target === "cursor" && h.value ? h.value : A.value ? A.el : Dn(e.target, o) || a.value), V = g(() => Array.isArray(R.value) ? void 0 : R.value);
  let _;
  return I(() => !!e.activator, (y) => {
    y && G ? (_ = ut(), _.run(() => {
      Qr(e, o, {
        activatorEl: a,
        activatorEvents: b
      });
    })) : _ && _.stop();
  }, {
    flush: "post",
    immediate: !0
  }), q(() => {
    _ == null || _.stop();
  }), {
    activatorEl: a,
    activatorRef: T,
    target: R,
    targetEl: V,
    targetRef: A,
    activatorEvents: b,
    contentEvents: w,
    scrimEvents: E
  };
}
function Qr(e, t, n) {
  let {
    activatorEl: i,
    activatorEvents: r
  } = n;
  I(() => e.activator, (l, u) => {
    if (u && l !== u) {
      const f = s(u);
      f && a(f);
    }
    l && de(() => o());
  }, {
    immediate: !0
  }), I(() => e.activatorProps, () => {
    o();
  }), q(() => {
    a();
  });
  function o() {
    let l = arguments.length > 0 && arguments[0] !== void 0 ? arguments[0] : s(), u = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : e.activatorProps;
    l && ki(l, z(r.value, u));
  }
  function a() {
    let l = arguments.length > 0 && arguments[0] !== void 0 ? arguments[0] : s(), u = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : e.activatorProps;
    l && Ei(l, z(r.value, u));
  }
  function s() {
    let l = arguments.length > 0 && arguments[0] !== void 0 ? arguments[0] : e.activator;
    const u = Dn(l, t);
    return i.value = (u == null ? void 0 : u.nodeType) === Node.ELEMENT_NODE ? u : void 0, i.value;
  }
}
function Dn(e, t) {
  var i, r;
  if (!e) return;
  let n;
  if (e === "parent") {
    let o = (r = (i = t == null ? void 0 : t.proxy) == null ? void 0 : i.$el) == null ? void 0 : r.parentNode;
    for (; o != null && o.hasAttribute("data-no-activator"); )
      o = o.parentNode;
    n = o;
  } else typeof e == "string" ? n = document.querySelector(e) : "$el" in e ? n = e.$el : n = e;
  return n;
}
const eo = Symbol.for("vuetify:display");
function to() {
  let e = arguments.length > 0 && arguments[0] !== void 0 ? arguments[0] : {}, t = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : Z();
  const n = J(eo);
  if (!n) throw new Error("Could not find Vuetify display injection");
  const i = g(() => {
    if (e.mobile != null) return e.mobile;
    if (!e.mobileBreakpoint) return n.mobile.value;
    const o = typeof e.mobileBreakpoint == "number" ? e.mobileBreakpoint : n.thresholds.value[e.mobileBreakpoint];
    return n.width.value < o;
  }), r = g(() => t ? {
    [`${t}--mobile`]: i.value
  } : {});
  return {
    ...n,
    displayClasses: r,
    mobile: i
  };
}
function no() {
  if (!G) return U(!1);
  const {
    ssr: e
  } = to();
  if (e) {
    const t = U(!1);
    return ct(() => {
      t.value = !0;
    }), t;
  } else
    return U(!0);
}
const io = S({
  eager: Boolean
}, "lazy");
function ro(e, t) {
  const n = U(!1), i = g(() => n.value || e.eager || t.value);
  I(t, () => n.value = !0);
  function r() {
    e.eager || (n.value = !1);
  }
  return {
    isBooted: n,
    hasContent: i,
    onAfterLeave: r
  };
}
function Wn() {
  const t = N("useScopeId").vnode.scopeId;
  return {
    scopeId: t ? {
      [t]: ""
    } : void 0
  };
}
const Zt = Symbol.for("vuetify:stack"), ge = le([]);
function oo(e, t, n) {
  const i = N("useStack"), r = !n, o = J(Zt, void 0), a = le({
    activeChildren: /* @__PURE__ */ new Set()
  });
  fe(Zt, a);
  const s = U(+t.value);
  De(e, () => {
    var v;
    const f = (v = ge.at(-1)) == null ? void 0 : v[1];
    s.value = f ? f + 10 : +t.value, r && ge.push([i.uid, s.value]), o == null || o.activeChildren.add(i.uid), q(() => {
      if (r) {
        const c = nn(ge).findIndex((m) => m[0] === i.uid);
        ge.splice(c, 1);
      }
      o == null || o.activeChildren.delete(i.uid);
    });
  });
  const l = U(!0);
  r && ce(() => {
    var v;
    const f = ((v = ge.at(-1)) == null ? void 0 : v[0]) === i.uid;
    setTimeout(() => l.value = f);
  });
  const u = g(() => !a.activeChildren.size);
  return {
    globalTop: rn(l),
    localTop: u,
    stackStyles: g(() => ({
      zIndex: s.value
    }))
  };
}
function ao(e) {
  return {
    teleportTarget: g(() => {
      const n = e();
      if (n === !0 || !G) return;
      const i = n === !1 ? document.body : typeof n == "string" ? document.querySelector(n) : n;
      if (i == null) {
        lt(`Unable to locate target ${n}`);
        return;
      }
      let r = [...i.children].find((o) => o.matches(".v-overlay-container"));
      return r || (r = document.createElement("div"), r.className = "v-overlay-container", i.appendChild(r)), r;
    })
  };
}
const so = S({
  transition: {
    type: [Boolean, String, Object],
    default: "fade-transition",
    validator: (e) => e !== !0
  }
}, "transition"), lo = (e, t) => {
  let {
    slots: n
  } = t;
  const {
    transition: i,
    disabled: r,
    group: o,
    ...a
  } = e, {
    component: s = o ? Qn : an,
    ...l
  } = typeof i == "object" ? i : {};
  return Zn(s, z(typeof i == "string" ? {
    name: r ? "" : i
  } : l, typeof i == "string" ? {} : Object.fromEntries(Object.entries({
    disabled: r,
    group: o
  }).filter((u) => {
    let [f, v] = u;
    return v !== void 0;
  })), a), n);
};
function uo() {
  return !0;
}
function zn(e, t, n) {
  if (!e || Hn(e, n) === !1) return !1;
  const i = mn(t);
  if (typeof ShadowRoot < "u" && i instanceof ShadowRoot && i.host === e.target) return !1;
  const r = (typeof n.value == "object" && n.value.include || (() => []))();
  return r.push(t), !r.some((o) => o == null ? void 0 : o.contains(e.target));
}
function Hn(e, t) {
  return (typeof t.value == "object" && t.value.closeConditional || uo)(e);
}
function co(e, t, n) {
  const i = typeof n.value == "function" ? n.value : n.value.handler;
  e.shadowTarget = e.target, t._clickOutside.lastMousedownWasOutside && zn(e, t, n) && setTimeout(() => {
    Hn(e, n) && i && i(e);
  }, 0);
}
function Qt(e, t) {
  const n = mn(e);
  t(document), typeof ShadowRoot < "u" && n instanceof ShadowRoot && t(n);
}
const fo = {
  // [data-app] may not be found
  // if using bind, inserted makes
  // sure that the root element is
  // available, iOS does not support
  // clicks on body
  mounted(e, t) {
    const n = (r) => co(r, e, t), i = (r) => {
      e._clickOutside.lastMousedownWasOutside = zn(r, e, t);
    };
    Qt(e, (r) => {
      r.addEventListener("click", n, !0), r.addEventListener("mousedown", i, !0);
    }), e._clickOutside || (e._clickOutside = {
      lastMousedownWasOutside: !1
    }), e._clickOutside[t.instance.$.uid] = {
      onClick: n,
      onMousedown: i
    };
  },
  beforeUnmount(e, t) {
    e._clickOutside && (Qt(e, (n) => {
      var o;
      if (!n || !((o = e._clickOutside) != null && o[t.instance.$.uid])) return;
      const {
        onClick: i,
        onMousedown: r
      } = e._clickOutside[t.instance.$.uid];
      n.removeEventListener("click", i, !0), n.removeEventListener("mousedown", r, !0);
    }), delete e._clickOutside[t.instance.$.uid]);
  }
};
function vo(e) {
  const {
    modelValue: t,
    color: n,
    ...i
  } = e;
  return p(an, {
    name: "fade-transition",
    appear: !0
  }, {
    default: () => [e.modelValue && p("div", z({
      class: ["v-overlay__scrim", e.color.backgroundColorClasses.value],
      style: e.color.backgroundColorStyles.value
    }, i), null)]
  });
}
const jn = S({
  absolute: Boolean,
  attach: [Boolean, String, Object],
  closeOnBack: {
    type: Boolean,
    default: !0
  },
  contained: Boolean,
  contentClass: null,
  contentProps: null,
  disabled: Boolean,
  opacity: [Number, String],
  noClickAnimation: Boolean,
  modelValue: Boolean,
  persistent: Boolean,
  scrim: {
    type: [Boolean, String],
    default: !0
  },
  zIndex: {
    type: [Number, String],
    default: 2e3
  },
  ...Jr(),
  ...Se(),
  ...On(),
  ...io(),
  ...Nr(),
  ...Hr(),
  ...xe(),
  ...so()
}, "VOverlay"), en = Y()({
  name: "VOverlay",
  directives: {
    ClickOutside: fo
  },
  inheritAttrs: !1,
  props: {
    _disableGlobalStack: Boolean,
    ...jn()
  },
  emits: {
    "click:outside": (e) => !0,
    "update:modelValue": (e) => !0,
    afterEnter: () => !0,
    afterLeave: () => !0
  },
  setup(e, t) {
    let {
      slots: n,
      attrs: i,
      emit: r
    } = t;
    const o = N("VOverlay"), a = B(), s = B(), l = B(), u = yt(e, "modelValue"), f = g({
      get: () => u.value,
      set: (O) => {
        O && e.disabled || (u.value = O);
      }
    }), {
      themeClasses: v
    } = ke(e), {
      rtlClasses: c,
      isRtl: m
    } = Ln(), {
      hasContent: h,
      onAfterLeave: d
    } = ro(e, f), b = Ui(g(() => typeof e.scrim == "string" ? e.scrim : null)), {
      globalTop: w,
      localTop: E,
      stackStyles: T
    } = oo(f, j(e, "zIndex"), e._disableGlobalStack), {
      activatorEl: A,
      activatorRef: R,
      target: V,
      targetEl: _,
      targetRef: y,
      activatorEvents: $,
      contentEvents: ve,
      scrimEvents: x
    } = Zr(e, {
      isActive: f,
      isTop: E,
      contentEl: l
    }), {
      teleportTarget: k
    } = ao(() => {
      var me, Ct, St;
      const O = e.attach || e.contained;
      if (O) return O;
      const H = ((me = A == null ? void 0 : A.value) == null ? void 0 : me.getRootNode()) || ((St = (Ct = o.proxy) == null ? void 0 : Ct.$el) == null ? void 0 : St.getRootNode());
      return H instanceof ShadowRoot ? H : !1;
    }), {
      dimensionStyles: C
    } = Tn(e), P = no(), {
      scopeId: F
    } = Wn();
    I(() => e.disabled, (O) => {
      O && (f.value = !1);
    });
    const {
      contentStyles: D,
      updateLocation: ne
    } = Mr(e, {
      isRtl: m,
      contentEl: l,
      target: V,
      isActive: f
    });
    jr(e, {
      root: a,
      contentEl: l,
      targetEl: _,
      isActive: f,
      updateLocation: ne
    });
    function M(O) {
      r("click:outside", O), e.persistent ? Ee() : f.value = !1;
    }
    function X(O) {
      return f.value && w.value && // If using scrim, only close if clicking on it rather than anything opened on top
      (!e.scrim || O.target === s.value || O instanceof MouseEvent && O.shadowTarget === s.value);
    }
    G && I(f, (O) => {
      O ? window.addEventListener("keydown", ie) : window.removeEventListener("keydown", ie);
    }, {
      immediate: !0
    }), Ce(() => {
      G && window.removeEventListener("keydown", ie);
    });
    function ie(O) {
      var H, me;
      O.key === "Escape" && w.value && (e.persistent ? Ee() : (f.value = !1, (H = l.value) != null && H.contains(document.activeElement) && ((me = A.value) == null || me.focus())));
    }
    const re = xr();
    De(() => e.closeOnBack, () => {
      Pr(re, (O) => {
        w.value && f.value ? (O(!1), e.persistent ? Ee() : f.value = !1) : O();
      });
    });
    const wt = B();
    I(() => f.value && (e.absolute || e.contained) && k.value == null, (O) => {
      if (O) {
        const H = Hi(a.value);
        H && H !== document.scrollingElement && (wt.value = H.scrollTop);
      }
    });
    function Ee() {
      e.noClickAnimation || l.value && xi(l.value, [{
        transformOrigin: "center"
      }, {
        transform: "scale(1.03)"
      }, {
        transformOrigin: "center"
      }], {
        duration: 150,
        easing: zi
      });
    }
    function Gn() {
      r("afterEnter");
    }
    function qn() {
      d(), r("afterLeave");
    }
    return te(() => {
      var O;
      return p(st, null, [(O = n.activator) == null ? void 0 : O.call(n, {
        isActive: f.value,
        targetRef: y,
        props: z({
          ref: R
        }, $.value, e.activatorProps)
      }), P.value && h.value && p(ei, {
        disabled: !k.value,
        to: k.value
      }, {
        default: () => [p("div", z({
          class: ["v-overlay", {
            "v-overlay--absolute": e.absolute || e.contained,
            "v-overlay--active": f.value,
            "v-overlay--contained": e.contained
          }, v.value, c.value, e.class],
          style: [T.value, {
            "--v-overlay-opacity": e.opacity,
            top: L(wt.value)
          }, e.style],
          ref: a
        }, F, i), [p(vo, z({
          color: b,
          modelValue: f.value && !!e.scrim,
          ref: s
        }, x.value), null), p(lo, {
          appear: !0,
          persisted: !0,
          transition: e.transition,
          target: V.value,
          onAfterEnter: Gn,
          onAfterLeave: qn
        }, {
          default: () => {
            var H;
            return [on(p("div", z({
              ref: l,
              class: ["v-overlay__content", e.contentClass],
              style: [C.value, D.value]
            }, ve.value, e.contentProps), [(H = n.default) == null ? void 0 : H.call(n, {
              isActive: f
            })]), [[ti, f.value], [ni("click-outside"), {
              handler: M,
              closeConditional: X,
              include: () => [A.value]
            }]])];
          }
        })])]
      })]);
    }), {
      activatorEl: A,
      scrimEl: s,
      target: V,
      animateClick: Ee,
      contentEl: l,
      globalTop: w,
      localTop: E,
      updateLocation: ne
    };
  }
}), Ke = Symbol("Forwarded refs");
function Je(e, t) {
  let n = e;
  for (; n; ) {
    const i = Reflect.getOwnPropertyDescriptor(n, t);
    if (i) return i;
    n = Object.getPrototypeOf(n);
  }
}
function mo(e) {
  for (var t = arguments.length, n = new Array(t > 1 ? t - 1 : 0), i = 1; i < t; i++)
    n[i - 1] = arguments[i];
  return e[Ke] = n, new Proxy(e, {
    get(r, o) {
      if (Reflect.has(r, o))
        return Reflect.get(r, o);
      if (!(typeof o == "symbol" || o.startsWith("$") || o.startsWith("__"))) {
        for (const a of n)
          if (a.value && Reflect.has(a.value, o)) {
            const s = Reflect.get(a.value, o);
            return typeof s == "function" ? s.bind(a.value) : s;
          }
      }
    },
    has(r, o) {
      if (Reflect.has(r, o))
        return !0;
      if (typeof o == "symbol" || o.startsWith("$") || o.startsWith("__")) return !1;
      for (const a of n)
        if (a.value && Reflect.has(a.value, o))
          return !0;
      return !1;
    },
    set(r, o, a) {
      if (Reflect.has(r, o))
        return Reflect.set(r, o, a);
      if (typeof o == "symbol" || o.startsWith("$") || o.startsWith("__")) return !1;
      for (const s of n)
        if (s.value && Reflect.has(s.value, o))
          return Reflect.set(s.value, o, a);
      return !1;
    },
    getOwnPropertyDescriptor(r, o) {
      var s;
      const a = Reflect.getOwnPropertyDescriptor(r, o);
      if (a) return a;
      if (!(typeof o == "symbol" || o.startsWith("$") || o.startsWith("__"))) {
        for (const l of n) {
          if (!l.value) continue;
          const u = Je(l.value, o) ?? ("_" in l.value ? Je((s = l.value._) == null ? void 0 : s.setupState, o) : void 0);
          if (u) return u;
        }
        for (const l of n) {
          const u = l.value && l.value[Ke];
          if (!u) continue;
          const f = u.slice();
          for (; f.length; ) {
            const v = f.shift(), c = Je(v.value, o);
            if (c) return c;
            const m = v.value && v.value[Ke];
            m && f.push(...m);
          }
        }
      }
    }
  });
}
const go = S({
  id: String,
  text: String,
  ...mi(jn({
    closeOnBack: !1,
    location: "end",
    locationStrategy: "connected",
    eager: !0,
    minWidth: 0,
    offset: 10,
    openOnClick: !1,
    openOnHover: !0,
    origin: "auto",
    scrim: !1,
    scrollStrategy: "reposition",
    transition: !1
  }), ["absolute", "persistent"])
}, "VTooltip"), ho = Y()({
  name: "VTooltip",
  props: go(),
  emits: {
    "update:modelValue": (e) => !0
  },
  setup(e, t) {
    let {
      slots: n
    } = t;
    const i = yt(e, "modelValue"), {
      scopeId: r
    } = Wn(), o = vt(), a = g(() => e.id || `v-tooltip-${o}`), s = B(), l = g(() => e.location.split(" ").length > 1 ? e.location : e.location + " center"), u = g(() => e.origin === "auto" || e.origin === "overlap" || e.origin.split(" ").length > 1 || e.location.split(" ").length > 1 ? e.origin : e.origin + " center"), f = g(() => e.transition ? e.transition : i.value ? "scale-transition" : "fade-transition"), v = g(() => z({
      "aria-describedby": a.value
    }, e.activatorProps));
    return te(() => {
      const c = en.filterProps(e);
      return p(en, z({
        ref: s,
        class: ["v-tooltip", e.class],
        style: e.style,
        id: a.value
      }, c, {
        modelValue: i.value,
        "onUpdate:modelValue": (m) => i.value = m,
        transition: f.value,
        absolute: !0,
        location: l.value,
        origin: u.value,
        persistent: !0,
        role: "tooltip",
        activatorProps: v.value,
        _disableGlobalStack: !0
      }, r), {
        activator: n.activator,
        default: function() {
          var b;
          for (var m = arguments.length, h = new Array(m), d = 0; d < m; d++)
            h[d] = arguments[d];
          return ((b = n.default) == null ? void 0 : b.call(n, ...h)) ?? e.text;
        }
      });
    }), mo({}, s);
  }
}), wo = /* @__PURE__ */ Ze({
  __name: "v-toggle-theme",
  props: {
    themeNameLight: { type: String, default: "light" },
    themeNameDark: { type: String, default: "dark" },
    fallbackLocation: {
      type: Object,
      default: () => ({ lat: 47.36667, lng: 8.55 })
    },
    tooltip: Boolean,
    tipDark: { type: String, default: "Dark mode" },
    tipLight: { type: String, default: "Light mode" },
    automatic: { type: Boolean, default: !0 },
    mdiJsIcons: { type: Boolean, default: !1 }
  },
  setup(e) {
    Ze({
      name: "VToggleTheme"
    });
    const t = e, n = B(t.automatic), i = si(), r = B(0), o = B(0), a = B(!1);
    let s;
    const l = {
      night: t.mdiJsIcons ? li : "mdi-weather-night",
      sunny: t.mdiJsIcons ? ui : "mdi-weather-sunny"
    }, u = () => {
      const c = (/* @__PURE__ */ new Date()).getTime(), m = ze(r.value, o.value).getTime(), h = xt(r.value, o.value).getTime();
      let d = c > h ? ze(r.value, o.value, new Date(c + 864e5)).getTime() : h;
      return c < m && (d = m), d - c;
    }, f = () => {
      if (n.value) {
        const c = (/* @__PURE__ */ new Date()).getTime(), m = ze(r.value, o.value).getTime(), h = xt(r.value, o.value).getTime();
        a.value = c < m || c > h, i.global.name.value = a.value ? t.themeNameDark : t.themeNameLight, s = setTimeout(f, u());
      }
    }, v = () => {
      n.value = !1, a.value = !a.value, i.global.name.value = a.value ? t.themeNameDark : t.themeNameLight, clearTimeout(s);
    };
    return ct(async () => {
      await de(), r.value = t.fallbackLocation.lat, o.value = t.fallbackLocation.lng, f();
    }), ii(() => {
      clearTimeout(s);
    }), I(() => t.automatic, (c) => {
      n.value = c, c ? f() : clearTimeout(s);
    }), (c, m) => (ri(), oi(Vr, {
      "aria-label": a.value ? e.tipDark : e.tipLight,
      icon: !0,
      onClick: v
    }, {
      default: ai(() => [
        p(_e, {
          icon: a.value ? l.night : l.sunny
        }, null, 8, ["icon"]),
        p(ho, {
          disabled: !e.tooltip,
          text: a.value ? e.tipDark : e.tipLight,
          activator: "parent",
          location: "start",
          "scroll-strategy": "none"
        }, null, 8, ["disabled", "text"])
      ]),
      _: 1
    }, 8, ["aria-label"]));
  }
});
export {
  wo as default
};
