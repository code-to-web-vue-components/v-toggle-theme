import { PropType } from 'vue';
declare const _default: import('vue').DefineComponent<import('vue').ExtractPropTypes<{
    themeNameLight: {
        type: StringConstructor;
        default: string;
    };
    themeNameDark: {
        type: StringConstructor;
        default: string;
    };
    fallbackLocation: {
        type: PropType<{
            lat: number;
            lng: number;
        }>;
        default: () => {
            lat: number;
            lng: number;
        };
    };
    tooltip: BooleanConstructor;
    tipDark: {
        type: StringConstructor;
        default: string;
    };
    tipLight: {
        type: StringConstructor;
        default: string;
    };
    automatic: {
        type: BooleanConstructor;
        default: boolean;
    };
    mdiJsIcons: {
        type: BooleanConstructor;
        default: boolean;
    };
}>, {}, {}, {}, {}, import('vue').ComponentOptionsMixin, import('vue').ComponentOptionsMixin, {}, string, import('vue').PublicProps, Readonly<import('vue').ExtractPropTypes<{
    themeNameLight: {
        type: StringConstructor;
        default: string;
    };
    themeNameDark: {
        type: StringConstructor;
        default: string;
    };
    fallbackLocation: {
        type: PropType<{
            lat: number;
            lng: number;
        }>;
        default: () => {
            lat: number;
            lng: number;
        };
    };
    tooltip: BooleanConstructor;
    tipDark: {
        type: StringConstructor;
        default: string;
    };
    tipLight: {
        type: StringConstructor;
        default: string;
    };
    automatic: {
        type: BooleanConstructor;
        default: boolean;
    };
    mdiJsIcons: {
        type: BooleanConstructor;
        default: boolean;
    };
}>> & Readonly<{}>, {
    themeNameLight: string;
    themeNameDark: string;
    fallbackLocation: {
        lat: number;
        lng: number;
    };
    tooltip: boolean;
    tipDark: string;
    tipLight: string;
    automatic: boolean;
    mdiJsIcons: boolean;
}, {}, {}, {}, string, import('vue').ComponentProvideOptions, true, {}, any>;
export default _default;
