import VToggleTheme from "/lib/components/v-toggle-theme.vue";

VToggleTheme.install = (app) => {
    app.component(VToggleTheme.name, VToggleTheme)
};
export default VToggleTheme;
